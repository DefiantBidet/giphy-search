const path = require('path');
const webpack = require('webpack');
const AutoPrefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const PostCSSFlexBox = require('postcss-flexbugs-fixes');
const CopyPlugin = require('copy-webpack-plugin');

const webpackDevConfig = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(process.cwd(), 'dist'),
    hot: true,
    port: 8888,
  },
  watch: true,
  entry: {
    app: [
      './src/js/App.js',
    ],
    vendors: [
      'react',
      'react-dom',
      'react-redux',
      'redux',
    ],
  },
  output: {
    filename: 'js/app-[name]-[hash].js',
    path: path.resolve(process.cwd(), 'dist'),
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.scss'],
    modules: ['src/js', 'src/scss', 'src/data', 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader', options: { sourceMap: true } },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: [
                PostCSSFlexBox(),
                AutoPrefixer(),
              ],
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: {
                localIdentName: 'app__[name]__[local]___[hash:base64:5]',
              },
              importLoaders: 2,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              ident: 'postcss',
              plugins: [
                PostCSSFlexBox(),
                AutoPrefixer(),
              ],
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                sourceMapContents: true,
                outputStyle: 'expanded',
                includePaths: [
                  path.resolve(__dirname, 'src/scss'),
                  path.resolve(__dirname, 'node_modules/'),
                ],
              },
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: 'file-loader',
      },
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/html/index.html',
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(true),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css',
      chunkFilename: '[id]-[hash].css',
    }),
    new OptimizeCssAssetsPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new CopyPlugin({
      patterns: [
        {
          context: './',
          from: 'assets/favicons/*',
          toType: 'dir',
          flatten: true,
        },
      ],
      options: {
        concurrency: 100,
      },
    }),
  ],
};

module.exports = webpackDevConfig;
