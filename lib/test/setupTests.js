// This sets up Jest prior to running tests.
// Enter configuration here if needed.

import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import 'core-js/stable';
import 'regenerator-runtime/runtime';

Enzyme.configure({ adapter: new Adapter() });

// Fail tests on any warning
console.error = (message) => {
  throw new Error(message);
};
