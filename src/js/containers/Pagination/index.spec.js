import React from 'react';
import { mount } from 'enzyme';

import PaginationContainer from '.';

const mockDispatch = jest.fn((action) => action);

jest.mock('react-redux', () => ({
  connect: (mapStateToProps, mapDispatchToProps) => (ReactComponent) => ({
    mapStateToProps,
    mapDispatchToProps: (dispatch = mockDispatch, ownProps) => mapDispatchToProps(dispatch, ownProps),
    ReactComponent,
  }),
  Provider: ({ children }) => children,
}));

const defaultPagination = {
  currentPage: null,
  perPage: null,
  totalPages: null,
};

const mockDefaultProps = {
  isLoading: false,
  showFavorites: false,
  searchPagination: defaultPagination,
  favoritesPagination: defaultPagination,
};

describe('<PaginationContainer />', () => {
  describe('snapshots', () => {
    it('matches - no pages defined & don\'t showFavorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - no pages defined & showFavorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = true;

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - default images pagination', () => {
      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = 1;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.searchPagination = mockPagination;

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - favorite images pagination', () => {
      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = 1;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = true;
      propsMock.favoritesPagination = mockPagination;

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('DOM structure', () => {
    it('renders a `nav` element as a base container', () => {
      const wrapper = mount(<PaginationContainer.ReactComponent {...mockDefaultProps} />);
      const navContainer = wrapper.childAt(0);

      expect(navContainer.type()).toBe('nav');
    });

    it('nav element has `navigation` role', () => {
      const wrapper = mount(<PaginationContainer.ReactComponent {...mockDefaultProps} />);
      const navContainer = wrapper.childAt(0);
      const navDomNode = navContainer.getDOMNode();

      expect(navDomNode.getAttribute('role')).toBe('navigation');
    });

    it('renders ReactPaginate if there is `totalPages` defined', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.searchPagination.currentPage = 1;
      propsMock.searchPagination.perPage = 20;
      propsMock.searchPagination.totalPages = 10;

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const navContainer = wrapper.childAt(0);
      const reactPaginate = navContainer.childAt(0);

      expect(reactPaginate.exists('ul.pagination')).toBe(true);
    });

    it('does not render ReactPaginate if `totalPages` is not defined', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const navContainer = wrapper.childAt(0);
      const reactPaginate = navContainer.childAt(0);

      expect(reactPaginate.exists('ul.pagination')).toBe(false);
    });
  });

  describe('interactions', () => {
    it('renders search pagination', () => {
      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = 1;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = false;
      propsMock.searchPagination = mockPagination;

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const pageButtons = wrapper.find('ul.pagination a[role=\'button\']');

      expect(pageButtons).toHaveLength(7);
    });

    it('renders favorites pagination', () => {
      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = 1;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = true;
      propsMock.favoritesPagination = mockPagination;

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const pageButtons = wrapper.find('ul.pagination a[role=\'button\']');

      expect(pageButtons).toHaveLength(7);
    });

    it('Clicking a page fires onPageClick', () => {
      const mockInitialPage = 1;
      const mockNextPage = 2;
      const onPageClickSpy = jest.spyOn(PaginationContainer.ReactComponent.prototype, 'onPageClick');
      const actionSpy = jest.fn();

      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = mockInitialPage;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.searchPagination = mockPagination;
      propsMock.actions = {
        onFavoritesPageChange: actionSpy,
        onSearchPageChange: actionSpy,
      };

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const pageButtons = wrapper.find('ul.pagination > li > a');

      expect(onPageClickSpy).toHaveBeenCalledTimes(0);

      pageButtons.last().simulate('click', { selected: mockNextPage });

      expect(onPageClickSpy).toHaveBeenCalledTimes(1);

      actionSpy.mockRestore();
      onPageClickSpy.mockRestore();
    });

    it('onPageClick dispatches page change action for search', () => {
      const mockInitialPage = 1;
      const mockNextPage = 2;
      const onPageClickSpy = jest.spyOn(PaginationContainer.ReactComponent.prototype, 'onPageClick');
      const onFavoritesPageChangeSpy = jest.fn();
      const onSearchPageChangeSpy = jest.fn();

      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = mockInitialPage;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = false;
      propsMock.searchPagination = mockPagination;
      propsMock.actions = {
        onFavoritesPageChange: onFavoritesPageChangeSpy,
        onSearchPageChange: onSearchPageChangeSpy,
      };

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const pageButtons = wrapper.find('ul.pagination > li > a');

      expect(onPageClickSpy).toHaveBeenCalledTimes(0);
      expect(onFavoritesPageChangeSpy).toHaveBeenCalledTimes(0);
      expect(onSearchPageChangeSpy).toHaveBeenCalledTimes(0);

      pageButtons.last().simulate('click', { selected: mockNextPage });

      expect(onPageClickSpy).toHaveBeenCalledTimes(1);
      expect(onFavoritesPageChangeSpy).toHaveBeenCalledTimes(0);
      expect(onSearchPageChangeSpy).toHaveBeenCalledTimes(1);

      onFavoritesPageChangeSpy.mockRestore();
      onSearchPageChangeSpy.mockRestore();
      onPageClickSpy.mockRestore();
    });

    it('onPageClick dispatches page change action for favorites', () => {
      const mockInitialPage = 1;
      const mockNextPage = 2;
      const onPageClickSpy = jest.spyOn(PaginationContainer.ReactComponent.prototype, 'onPageClick');
      const onFavoritesPageChangeSpy = jest.fn();
      const onSearchPageChangeSpy = jest.fn();

      const mockPagination = JSON.parse(JSON.stringify(defaultPagination));
      mockPagination.currentPage = mockInitialPage;
      mockPagination.perPage = 20;
      mockPagination.totalPages = 5;

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = true;
      propsMock.favoritesPagination = mockPagination;
      propsMock.actions = {
        onFavoritesPageChange: onFavoritesPageChangeSpy,
        onSearchPageChange: onSearchPageChangeSpy,
      };

      const wrapper = mount(<PaginationContainer.ReactComponent {...propsMock} />);
      const pageButtons = wrapper.find('ul.pagination > li > a');

      expect(onPageClickSpy).toHaveBeenCalledTimes(0);
      expect(onFavoritesPageChangeSpy).toHaveBeenCalledTimes(0);
      expect(onSearchPageChangeSpy).toHaveBeenCalledTimes(0);

      pageButtons.last().simulate('click', { selected: mockNextPage });

      expect(onPageClickSpy).toHaveBeenCalledTimes(1);
      expect(onFavoritesPageChangeSpy).toHaveBeenCalledTimes(1);
      expect(onSearchPageChangeSpy).toHaveBeenCalledTimes(0);

      onFavoritesPageChangeSpy.mockRestore();
      onSearchPageChangeSpy.mockRestore();
      onPageClickSpy.mockRestore();
    });
  });
});
