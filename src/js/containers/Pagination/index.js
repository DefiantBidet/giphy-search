import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { onPageChange as onFavoritesPageChange } from 'store/ducks/favorites';
import { onPageChange as onSearchPageChange } from 'store/ducks/search';

import styles from 'containers/pagination.scss';

class PaginationContainer extends Component {
  constructor(props) {
    super(props);

    this.onPageClick = this.onPageClick.bind(this);
  }

  onPageClick(event) {
    const selectedPage = event.selected;
    const { actions, showFavorites } = this.props;

    const pageChangeAction = showFavorites
      ? actions.onFavoritesPageChange
      : actions.onSearchPageChange;

    pageChangeAction(selectedPage);
  }

  render() {
    const {
      searchPagination,
      favoritesPagination,
      showFavorites,
    } = this.props;

    const pagination = showFavorites
      ? favoritesPagination
      : searchPagination;

    return (
      <nav className={styles.containerBase} role='navigation'>
        {pagination.totalPages && (
          <ReactPaginate
            previousLabel='<'
            nextLabel='>'
            breakLabel='...'
            breakClassName='break'
            pageCount={pagination.totalPages}
            marginPagesDisplayed={1}
            pageRangeDisplayed={2}
            onPageChange={this.onPageClick}
            containerClassName='pagination'
            subContainerClassName='pages pagination'
            activeClassName='active'
          />)}
      </nav>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      onFavoritesPageChange,
      onSearchPageChange,
    }, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    isLoading: state.search.isLoading,
    searchPagination: state.search.pagination,
    favoritesPagination: state.favorites.pagination,
    showFavorites: state.favorites.showFavorites,
  };
}

const ConnectedPaginationContainer = connect(mapStateToProps, mapDispatchToProps)(PaginationContainer);

export default ConnectedPaginationContainer;
