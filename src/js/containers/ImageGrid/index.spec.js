import React from 'react';
import { mount } from 'enzyme';

import Image from 'components/Image';
import ImageGridContainer from '.';

const mockDispatch = jest.fn((action) => action);

jest.mock('react-redux', () => ({
  connect: (mapStateToProps, mapDispatchToProps) => (ReactComponent) => ({
    mapStateToProps,
    mapDispatchToProps: (dispatch = mockDispatch, ownProps) => mapDispatchToProps(dispatch, ownProps),
    ReactComponent,
  }),
  Provider: ({ children }) => children,
}));

const mockDefaultProps = {
  isLoading: false,
  showFavorites: false,
  imageList: [],
  favoritesList: [],
};

const mockList = [
  {
    id: '976b6d87-8097-4027-bd7b-06de4d5ec22b',
    title: 'tragal explantation',
    slug: 'coloptosis-profluvious',
    url: 'https://via.placeholder.com/250x200',
  },
  {
    id: 'a89db39f-c5ad-44bb-b69f-0c3baf27945b',
    title: 'manipular waxer',
    slug: 'hygienization-hyponome',
    url: 'https://via.placeholder.com/250x200',
  },
  {
    id: '9663a5f1-ca6d-4748-8fcc-356cda9decad',
    title: 'eldersisterly dreggy',
    slug: 'intratubal-makeshifty',
    url: 'https://via.placeholder.com/250x200',
  },
];

describe('<ImageGridContainer />', () => {
  describe('snapshots', () => {
    it('matches - no images & search images list', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - no images & favoritesList', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.showFavorites = true;

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - is loading', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.isLoading = true;

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - list of images', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.imageList = mockList;

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('DOM structure', () => {
    it('renders a `div` element as a base container', () => {
      const wrapper = mount(<ImageGridContainer.ReactComponent {...mockDefaultProps} />);
      const imageGrid = wrapper.childAt(0);

      expect(imageGrid.type()).toBe('div');
    });

    it('renders a `div` element for an empty list', () => {
      const expectedCopy = 'No Results';
      const wrapper = mount(<ImageGridContainer.ReactComponent {...mockDefaultProps} />);
      const imageGrid = wrapper.childAt(0);
      const emptyContainer = imageGrid.childAt(0);

      expect(emptyContainer.type()).toBe('div');
      expect(emptyContainer.hasClass('emptyList')).toBe(true);
      expect(emptyContainer.text()).toEqual(expectedCopy);
    });

    it('renders a `div` element for an image list', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.isLoading = false;
      propsMock.imageList = mockList;

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      const imageGrid = wrapper.childAt(0);
      const grid = imageGrid.childAt(0);

      expect(grid.type()).toBe('div');
      expect(grid.hasClass('imageList')).toBe(true);
    });

    it('image list has child Image components', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.isLoading = false;
      propsMock.imageList = mockList;

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      const imageGrid = wrapper.childAt(0);
      const grid = imageGrid.childAt(0);
      const images = grid.children();

      images.forEach((child) => {
        expect(child.instance()).toBeInstanceOf(Image);
      });
    });

    it('renders a `div` element for a loading overlay', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.isLoading = true;
      propsMock.imageList = mockList;

      const expectedCopy = 'Loading...';
      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      const imageGrid = wrapper.childAt(0);
      const loadingOverlay = imageGrid.last();

      expect(loadingOverlay.type()).toBe('div');
      expect(loadingOverlay.text()).toEqual(expectedCopy);
    });
  });

  describe('interactions', () => {
    it('Clicking an Image fires onImageClick - adds to favorites', () => {
      const spy = jest.spyOn(ImageGridContainer.ReactComponent.prototype, 'onImageClick');
      const onRemoveFavoriteSpy = jest.fn();
      const onSaveFavoriteSpy = jest.fn();

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.imageList = mockList;
      propsMock.actions = {
        onRemoveFavorite: onRemoveFavoriteSpy,
        onSaveFavorite: onSaveFavoriteSpy,
      };

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      const imageGrid = wrapper.childAt(0);
      const grid = imageGrid.childAt(0);
      const image = grid.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);
      expect(onRemoveFavoriteSpy).toHaveBeenCalledTimes(0);
      expect(onSaveFavoriteSpy).toHaveBeenCalledTimes(0);

      image.simulate('click', { target: { test: 0 } });

      expect(spy).toHaveBeenCalledTimes(1);
      expect(onRemoveFavoriteSpy).toHaveBeenCalledTimes(0);
      expect(onSaveFavoriteSpy).toHaveBeenCalledTimes(1);

      onRemoveFavoriteSpy.mockRestore();
      onSaveFavoriteSpy.mockRestore();
      spy.mockRestore();
    });

    it('Clicking an Image fires onImageClick - removes from favorites', () => {
      const spy = jest.spyOn(ImageGridContainer.ReactComponent.prototype, 'onImageClick');
      const onRemoveFavoriteSpy = jest.fn();
      const onSaveFavoriteSpy = jest.fn();

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.imageList = mockList;
      propsMock.favoritesList.push(mockList[0]);
      propsMock.actions = {
        onRemoveFavorite: onRemoveFavoriteSpy,
        onSaveFavorite: onSaveFavoriteSpy,
      };

      const wrapper = mount(<ImageGridContainer.ReactComponent {...propsMock} />);
      const imageGrid = wrapper.childAt(0);
      const grid = imageGrid.childAt(0);
      const image = grid.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);
      expect(onRemoveFavoriteSpy).toHaveBeenCalledTimes(0);
      expect(onSaveFavoriteSpy).toHaveBeenCalledTimes(0);

      image.simulate('click', { target: { test: 0 } });

      expect(spy).toHaveBeenCalledTimes(1);
      expect(onRemoveFavoriteSpy).toHaveBeenCalledTimes(1);
      expect(onSaveFavoriteSpy).toHaveBeenCalledTimes(0);

      onRemoveFavoriteSpy.mockRestore();
      onSaveFavoriteSpy.mockRestore();
      spy.mockRestore();
    });
  });
});
