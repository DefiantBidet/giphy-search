import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { onChangeLoading } from 'store/ducks/search';
import { onSaveFavorite, onRemoveFavorite } from 'store/ducks/favorites';
import Image from 'components/Image';

import styles from 'containers/image-grid.scss';

class ImageGridContainer extends Component {
  constructor(props) {
    super(props);

    this.loaded = new Set();

    this.onImageLoaded = this.onImageLoaded.bind(this);
    this.onImageClick = this.onImageClick.bind(this);
  }

  onImageClick(imageId) {
    const { actions, favoritesList, imageList } = this.props;
    const clickedImage = imageList.find((gifObject) => gifObject.id === imageId);
    const exists = favoritesList.some((gif) => gif.id === imageId);

    if (exists) {
      // already a favorite ... remove
      actions.onRemoveFavorite(imageId);
      return;
    }

    // add to favorites
    actions.onSaveFavorite(clickedImage);
  }

  onImageLoaded(imageId) {
    const { actions } = this.props;
    const list = this.listToRender();

    this.loaded.add(imageId);

    if (this.loaded.size >= list.length - 1) {
      actions.onChangeLoading(false);
    }
  }

  renderEmptyList() {
    this.loaded = new Set();

    return (
      <div className={styles.emptyList}>
        No Results
      </div>
    );
  }

  addLoadingOverlay() {
    this.loaded = new Set();

    return (
      <div className={styles.loadingOverlay}>
        Loading...
      </div>
    );
  }

  renderImageList() {
    const {
      isLoading,
      favoritesList,
      showFavorites,
    } = this.props;

    this.loaded = new Set();
    const list = this.listToRender();

    const gifs = list.map((gifObject) => {
      const isFavorite = showFavorites || favoritesList.some((favObj) => favObj.id === gifObject.id);

      return (
        <Image
          key={gifObject.id}
          isFavorite={isFavorite}
          onLoaded={this.onImageLoaded}
          onClick={this.onImageClick}
          imageId={gifObject.id}
          imageUrl={gifObject.url}
        />
      );
    });

    const classList = isLoading ? `${styles.imageList} ${styles.isLoading}` : styles.imageList;
    return (
      <div className={classList}>
        {gifs}
      </div>
    );
  }

  listToRender() {
    const {
      imageList,
      favoritesList,
      showFavorites,
    } = this.props;

    const list = showFavorites ? favoritesList : imageList;
    return list;
  }

  render() {
    const { isLoading } = this.props;
    const list = this.listToRender();

    return (
      <div className={styles.imageContainer}>
        {list.length === 0 && this.renderEmptyList()}
        {list.length > 0 && this.renderImageList()}
        {isLoading && this.addLoadingOverlay()}
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      onChangeLoading,
      onSaveFavorite,
      onRemoveFavorite,
    }, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    isLoading: state.search.isLoading,
    imageList: state.search.searchResults,
    showFavorites: state.favorites.showFavorites,
    favoritesList: state.favorites.saved,
  };
}

const ConnectedImageGridContainer = connect(mapStateToProps, mapDispatchToProps)(ImageGridContainer);

export default ConnectedImageGridContainer;
