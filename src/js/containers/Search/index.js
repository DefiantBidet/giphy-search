import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SearchButton from 'components/Buttons/SearchButton';
import TextInput from 'components/TextInput';
import {
  onUpdateSearchString,
  onSearchRequest,
  onSearchRequestLoaded,
  onSearchRequestError,
  onChangeLoading,
} from 'store/ducks/search';
import { onShowFavorites } from 'store/ducks/favorites';
import debounce from 'utils/debounce';

import styles from 'containers/search.scss';

class SearchContainer extends Component {
  constructor(props) {
    super(props);
    this.inputString = '';

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onSearchClick = this.onSearchClick.bind(this);
    this.onShowFavorites = this.onShowFavorites.bind(this);
    this.onSearchChange = debounce(this.onSearchChange.bind(this), 275);
  }

  onFormSubmit(event) {
    event.preventDefault();
    event.stopPropagation();
    const { actions } = this.props;
    actions.onSearchRequest();
  }

  onSearchClick(event) {
    event.preventDefault();
    const { actions } = this.props;
    actions.onSearchRequest();
  }

  onShowFavorites(event) {
    event.preventDefault();
    event.stopPropagation();

    const { actions, favorites } = this.props;
    if (favorites.saved.length > 0) {
      actions.onChangeLoading(true);
      actions.onShowFavorites();
    }
  }

  onSearchChange(value) {
    const { actions, search: { searchString } } = this.props;
    if (value !== searchString) {
      actions.onUpdateSearchString(value);
    }
  }

  render() {
    const { favorites, search } = this.props;
    const labelClassList = favorites.saved.length > 0
      ? styles.favoritesLabel
      : `${styles.favoritesLabel} ${styles.disabledFavorites}`;

    return (
      <div className={styles.containerBase}>
        <form className={styles.form} role='search' onSubmit={this.onFormSubmit}>
          <div role='group' className={styles.fieldset}>
            <TextInput
              elementName='search-input'
              placeHolder='Search GIFs'
              label='search for GIFs'
              inputValue={search.searchString}
              onInput={this.onSearchChange}
            />
            <SearchButton
              ariaLabel='Search'
              loading={search.isLoading}
              onClick={this.onSearchClick}
            />
          </div>
          <div role='group' className={styles.fieldset}>
            <label className={labelClassList} onClick={this.onShowFavorites}>
              <input
                type='checkbox'
                name='checkbox'
                checked={favorites.showFavorites}
                onChange={this.onShowFavorites}
              />
              Show Favorites
            </label>
          </div>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      onChangeLoading,
      onUpdateSearchString,
      onSearchRequest,
      onSearchRequestLoaded,
      onSearchRequestError,
      onShowFavorites,
    }, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    search: state.search,
    favorites: state.favorites,
  };
}

const ConnectedSearchContainer = connect(mapStateToProps, mapDispatchToProps)(SearchContainer);

export default ConnectedSearchContainer;
