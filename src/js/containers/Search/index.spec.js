import React from 'react';
import { mount } from 'enzyme';

import SearchButton from 'components/Buttons/SearchButton';
import TextInput from 'components/TextInput';
import SearchContainer from '.';

const mockDispatch = jest.fn((action) => action);

jest.mock('react-redux', () => ({
  connect: (mapStateToProps, mapDispatchToProps) => (ReactComponent) => ({
    mapStateToProps,
    mapDispatchToProps: (dispatch = mockDispatch, ownProps) => mapDispatchToProps(dispatch, ownProps),
    ReactComponent,
  }),
  Provider: ({ children }) => children,
}));

const mockDefaultProps = {
  search: {
    isLoading: false,
    searchString: '',
    searchResults: [],
  },
  favorites: {
    saved: [],
    showFavorites: false,
  },
};

const mockList = [
  {
    id: '976b6d87-8097-4027-bd7b-06de4d5ec22b',
    title: 'tragal explantation',
    slug: 'coloptosis-profluvious',
    url: 'https://via.placeholder.com/250x200',
  },
  {
    id: 'a89db39f-c5ad-44bb-b69f-0c3baf27945b',
    title: 'manipular waxer',
    slug: 'hygienization-hyponome',
    url: 'https://via.placeholder.com/250x200',
  },
  {
    id: '9663a5f1-ca6d-4748-8fcc-356cda9decad',
    title: 'eldersisterly dreggy',
    slug: 'intratubal-makeshifty',
    url: 'https://via.placeholder.com/250x200',
  },
];

function createMockEvent() {
  const preventDefaultSpy = jest.fn();
  const stopPropagationSpy = jest.fn();

  const mockEvent = {
    preventDefault: preventDefaultSpy,
    stopPropagation: stopPropagationSpy,
  };

  return {
    event: mockEvent,
    spies: {
      preventDefaultSpy,
      stopPropagationSpy,
    },
  };
}

describe('<SearchContainer />', () => {
  describe('snapshots', () => {
    it('matches - not loading & no favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.isLoading = false;
      propsMock.favorites.saved = [];

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - not loading & favorites & don\'t show favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.isLoading = false;
      propsMock.favorites.saved = mockList;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - not loading & has favorites saved & show favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.isLoading = false;
      propsMock.favorites.showFavorites = true;
      propsMock.favorites.saved = mockList;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - is loading & no favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.isLoading = true;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - is loading & favorites  & don\'t show favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.isLoading = true;
      propsMock.favorites.saved = mockList;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches - is loading & has favorites saved  & show favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.isLoading = true;
      propsMock.favorites.showFavorites = true;
      propsMock.favorites.saved = mockList;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('DOM structure', () => {
    it('renders a `div` element as a base container', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const searchContainer = wrapper.childAt(0);

      expect(searchContainer.type()).toBe('div');
    });

    it('renders a `form` element', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const searchContainer = wrapper.childAt(0);
      const form = searchContainer.childAt(0);

      expect(form.type()).toBe('form');
    });

    it('form element has `search` role', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const formDomNode = form.getDOMNode();

      expect(form.type()).toBe('form');
      expect(formDomNode.getAttribute('role')).toBe('search');
    });

    it('renders a `div` element acting as a fieldset - containing search', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(0);
      const fieldsetDomNode = fieldset.getDOMNode();

      expect(form.type()).toBe('form');
      expect(fieldsetDomNode.getAttribute('role')).toBe('group');
    });

    it('search fieldset renders a TextInput', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(0);
      const textInput = fieldset.childAt(0);

      expect(textInput.instance()).toBeInstanceOf(TextInput);
    });

    it('search fieldset renders a SearchButton', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(0);
      const searchButton = fieldset.childAt(1);

      expect(searchButton.instance()).toBeInstanceOf(SearchButton);
    });

    it('renders a `div` element acting as a fieldset - containing favorites', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(1);
      const fieldsetDomNode = fieldset.getDOMNode();

      expect(form.type()).toBe('form');
      expect(fieldsetDomNode.getAttribute('role')).toBe('group');
    });

    it('favorites fieldset renders a `label` element', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(1);
      const label = fieldset.childAt(0);

      expect(label.type()).toBe('label');
    });

    it('favorites `label` is disabled when there are no favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.favorites.saved = [];

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(1);
      const label = fieldset.childAt(0);

      expect(label.hasClass('favoritesLabel')).toBeTruthy();
      expect(label.hasClass('disabledFavorites')).toBeTruthy();
    });

    it('favorites `label` is not disabled when there are favorites', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.favorites.saved = mockList;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(1);
      const label = fieldset.childAt(0);

      expect(label.hasClass('favoritesLabel')).toBeTruthy();
      expect(label.hasClass('disabledFavorites')).toBeFalsy();
    });

    it('favorites `label` renders an unchecked checkbox', () => {
      const wrapper = mount(<SearchContainer.ReactComponent {...mockDefaultProps} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(1);
      const label = fieldset.childAt(0);
      const input = label.childAt(0);
      const inputDomNode = input.getDOMNode();

      expect(input.prop('checked')).toBeFalsy();
      expect(inputDomNode.getAttribute('type')).toBe('checkbox');
    });

    it('favorites `label` renders a checked checkbox', () => {
      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.favorites.showFavorites = true;
      propsMock.favorites.saved = mockList;

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      const form = wrapper.find('form');
      const fieldset = form.childAt(1);
      const label = fieldset.childAt(0);
      const input = label.childAt(0);
      const inputDomNode = input.getDOMNode();

      expect(input.prop('checked')).toBeTruthy();
      expect(inputDomNode.getAttribute('type')).toBe('checkbox');
    });
  });

  describe('interactions', () => {
    it('onFormSubmit fired via `enter` keypress', () => {
      const formSubmitSpy = jest.spyOn(SearchContainer.ReactComponent.prototype, 'onFormSubmit');
      const onSearchRequestSpy = jest.fn();

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.searchString = 'fooooooooooo!';
      propsMock.actions = {
        onSearchRequest: onSearchRequestSpy,
      };

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      const form = wrapper.find('form');

      const mockEvent = createMockEvent();

      expect(formSubmitSpy).toHaveBeenCalledTimes(0);
      expect(onSearchRequestSpy).toHaveBeenCalledTimes(0);
      expect(mockEvent.spies.preventDefaultSpy).toHaveBeenCalledTimes(0);
      expect(mockEvent.spies.stopPropagationSpy).toHaveBeenCalledTimes(0);

      form.simulate('submit', mockEvent.event);

      expect(formSubmitSpy).toHaveBeenCalledTimes(1);
      expect(onSearchRequestSpy).toHaveBeenCalledTimes(1);
      expect(mockEvent.spies.preventDefaultSpy).toHaveBeenCalledTimes(1);
      expect(mockEvent.spies.stopPropagationSpy).toHaveBeenCalledTimes(1);

      mockEvent.spies.preventDefaultSpy.mockRestore();
      mockEvent.spies.stopPropagationSpy.mockRestore();
      onSearchRequestSpy.mockRestore();
      formSubmitSpy.mockRestore();
    });

    it('onSearchClick fired on SearchButton click', () => {
      const onSearchClickSpy = jest.spyOn(SearchContainer.ReactComponent.prototype, 'onSearchClick');
      const onSearchRequestSpy = jest.fn();

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.search.searchString = 'fooooooooooo!';
      propsMock.actions = {
        onSearchRequest: onSearchRequestSpy,
      };

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      const form = wrapper.find('form');
      const button = form.find('button[type=\'button\']');

      const mockEvent = createMockEvent();

      expect(onSearchClickSpy).toHaveBeenCalledTimes(0);
      expect(onSearchRequestSpy).toHaveBeenCalledTimes(0);
      expect(mockEvent.spies.preventDefaultSpy).toHaveBeenCalledTimes(0);
      expect(mockEvent.spies.stopPropagationSpy).toHaveBeenCalledTimes(0);

      button.simulate('click', mockEvent.event);

      expect(onSearchClickSpy).toHaveBeenCalledTimes(1);
      expect(onSearchRequestSpy).toHaveBeenCalledTimes(1);
      expect(mockEvent.spies.preventDefaultSpy).toHaveBeenCalledTimes(1);
      expect(mockEvent.spies.stopPropagationSpy).toHaveBeenCalledTimes(1);

      mockEvent.spies.preventDefaultSpy.mockRestore();
      mockEvent.spies.stopPropagationSpy.mockRestore();
      onSearchRequestSpy.mockRestore();
      onSearchClickSpy.mockRestore();
    });

    it('onShowFavorites fired when checkbox toggled', () => {
      const onShowFavoritesSpy = jest.spyOn(SearchContainer.ReactComponent.prototype, 'onShowFavorites');
      const onChangeLoadingActionSpy = jest.fn();
      const onShowFavoritesActionSpy = jest.fn();

      const propsMock = JSON.parse(JSON.stringify(mockDefaultProps));
      propsMock.favorites.saved = mockList;
      propsMock.actions = {
        onChangeLoading: onChangeLoadingActionSpy,
        onShowFavorites: onShowFavoritesActionSpy,
      };

      const wrapper = mount(<SearchContainer.ReactComponent {...propsMock} />);
      const form = wrapper.find('form');
      const checkbox = form.find('input[type=\'checkbox\']');

      const mockEvent = createMockEvent();

      expect(onShowFavoritesSpy).toHaveBeenCalledTimes(0);
      expect(onChangeLoadingActionSpy).toHaveBeenCalledTimes(0);
      expect(onShowFavoritesActionSpy).toHaveBeenCalledTimes(0);
      expect(mockEvent.spies.preventDefaultSpy).toHaveBeenCalledTimes(0);
      expect(mockEvent.spies.stopPropagationSpy).toHaveBeenCalledTimes(0);

      checkbox.simulate('click', mockEvent.event);

      expect(onShowFavoritesSpy).toHaveBeenCalledTimes(1);
      expect(onChangeLoadingActionSpy).toHaveBeenCalledTimes(1);
      expect(onChangeLoadingActionSpy).toHaveBeenCalledWith(true);
      expect(onShowFavoritesActionSpy).toHaveBeenCalledTimes(1);
      expect(mockEvent.spies.preventDefaultSpy).toHaveBeenCalledTimes(1);
      expect(mockEvent.spies.stopPropagationSpy).toHaveBeenCalledTimes(1);

      mockEvent.spies.preventDefaultSpy.mockRestore();
      mockEvent.spies.stopPropagationSpy.mockRestore();
      onChangeLoadingActionSpy.mockRestore();
      onShowFavoritesActionSpy.mockRestore();
      onShowFavoritesSpy.mockRestore();
    });

    // it.skip('onSearchChange is a debounced handler of onInput changes', () => {});
  });
});
