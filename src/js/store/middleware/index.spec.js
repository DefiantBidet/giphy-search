import * as MIDDLEWARE from '.';

describe('redux middleware', () => {
  it('exports loggerMiddleware', () => {
    expect(MIDDLEWARE).toEqual(
      expect.objectContaining({
        loggerMiddleware: expect.any(Function),
      }),
    );
  });

  it('exports sagaMiddleware', () => {
    expect(MIDDLEWARE).toEqual(
      expect.objectContaining({
        sagaMiddleware: expect.any(Function),
      }),
    );
  });
});
