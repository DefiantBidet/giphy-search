import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

export const loggerMiddleware = createLogger();
export const sagaMiddleware = createSagaMiddleware();

// any other middleware...
