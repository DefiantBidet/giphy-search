import {
  applyMiddleware,
  combineReducers,
  createStore,
} from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension';

import reducers, { rootSaga } from './ducks';
import * as MIDDLEWARE from './middleware';

function configureStore() {
  const allMiddleware = Object.values(MIDDLEWARE);
  const rootReducer = combineReducers({
    favorites: reducers.favorites,
    search: reducers.search,
  });

  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...allMiddleware)),
  );

  MIDDLEWARE.sagaMiddleware.run(rootSaga);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./ducks', () => {
      // eslint-disable-next-line global-require
      const nextReducer = require('./ducks');
      store.replaceReducer(combineReducers({ ...nextReducer }));
    });
  }

  return store;
}

export default configureStore();
