import {
  createPaginationObject,
  calculateFavoritePagination,
} from './utils/pagination';

/*
********************************************************************************
**                               Initial State                                **
********************************************************************************
*/
const INITIAL_STATE = {
  showFavorites: false,
  saved: [],
  pagination: createPaginationObject(),
};

/*
********************************************************************************
**                               Helper Methos                                **
********************************************************************************
*/
function favoriteModel(data) {
  return {
    ...data,
    created_at: Date.now(),
  };
}

/*
********************************************************************************
**                                   Actions                                  **
********************************************************************************
*/
const ON_SAVE_FAVORITE = 'favorites/ON_SAVE_GIF';
const ON_REMOVE_FAVORITE = 'favorites/ON_REMOVE_GIF';
const ON_SHOW_FAVORITES = 'favorites/ON_SHOW_FAVORITES';
const ON_PAGE_CHANGE = 'favorites/ON_PAGE_CHANGE';

const actions = {
  ON_SAVE_FAVORITE,
  ON_REMOVE_FAVORITE,
  ON_SHOW_FAVORITES,
  ON_PAGE_CHANGE,
};

/*
********************************************************************************
**                                   Redcuer                                  **
********************************************************************************
*/
function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case ON_SAVE_FAVORITE: {
      const { saved } = state;
      const favoriteObj = action.data;

      // immutable array pattern
      const favoritesList = saved.slice();
      favoritesList.splice(favoritesList.length, 0, favoriteObj);

      const pagination = calculateFavoritePagination(favoritesList);

      return {
        ...state,
        pagination,
        saved: favoritesList,
      };
    }

    case ON_REMOVE_FAVORITE: {
      const { saved, showFavorites } = state;
      const removeId = action.data;
      const favoriteIndex = saved.findIndex((favorite) => favorite.id === removeId);

      // immutable array pattern
      const favoritesList = saved.slice();

      if (favoriteIndex >= 0) {
        favoritesList.splice(favoriteIndex, 1);
      }

      const displayFavs = (favoritesList.length > 0) ? showFavorites : false;

      return {
        ...state,
        showFavorites: displayFavs,
        saved: favoritesList,
      };
    }

    case ON_SHOW_FAVORITES: {
      const { showFavorites } = state;
      const toggledValue = !showFavorites;

      return {
        ...state,
        showFavorites: toggledValue,
      };
    }

    case ON_PAGE_CHANGE: {
      const { pagination: statePagination } = state;

      // deep clone of state object for immutability
      const pagination = JSON.parse(JSON.stringify(statePagination));
      pagination.currentPage = action.data;

      const isLoading = true;

      return {
        ...state,
        isLoading,
        pagination,
      };
    }

    default: return state;
  }
}

/*
********************************************************************************
**                               Action Creators                              **
********************************************************************************
*/
function onSaveFavorite(data) {
  const newFavoriteModel = favoriteModel({
    id: data.id,
    slug: data.slug,
    url: data.url,
  });
  return {
    type: ON_SAVE_FAVORITE,
    data: newFavoriteModel,
  };
}

function onRemoveFavorite(data) {
  return {
    type: ON_REMOVE_FAVORITE,
    data,
  };
}

function onShowFavorites() {
  return {
    type: ON_SHOW_FAVORITES,
  };
}

function onPageChange(page) {
  return {
    type: ON_PAGE_CHANGE,
    data: page,
  };
}

/*
********************************************************************************
**                                    Sagas                                   **
********************************************************************************
*/

const sagas = [];

/*
********************************************************************************
**                                  Exports                                   **
********************************************************************************
*/
export {
  actions,
  onSaveFavorite,
  onRemoveFavorite,
  onShowFavorites,
  onPageChange,
  sagas,
};

export default reducer;
