import { all, call, spawn } from 'redux-saga/effects';
import favorites, { sagas as favoriteSagas } from './favorites';
import search, { sagas as searchSagas } from './search';

// Reducers
const reducers = {
  search,
  favorites,
};

function* initializeSaga(saga) {
  let initError = false;

  while (!initError) {
    initError = true;
    try {
      // eslint-disable-next-line no-loop-func
      setTimeout(() => { initError = false; });
      yield call(saga);
    } catch (error) {
      if (initError) {
        throw new Error(`${saga.name} :: terminated due to an exception initializing.`);
      }
    }
  }
}

function* rootSaga() {
  const sagas = [
    ...searchSagas,
    ...favoriteSagas,
  ];

  yield all(sagas.map((saga) => spawn(initializeSaga, saga)));
}

export default reducers;
export { rootSaga };
