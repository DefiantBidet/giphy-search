import reducers, { rootSaga } from '.';
import favorites from './favorites';
import search from './search';

function isGenerator(obj) {
  return typeof obj.next === 'function' && typeof obj.throw === 'function';
}

function isGeneratorFunction(obj) {
  const constructorReference = obj.constructor;

  if (!constructorReference) {
    return false;
  }

  if (
    constructorReference.name === 'GeneratorFunction'
    || constructorReference.displayName === 'GeneratorFunction'
  ) {
    return true;
  }

  return isGenerator(constructorReference.prototype);
}

describe('ducks main entry point', () => {
  it('exports all ducks reducers', () => {
    expect(reducers).toEqual(
      expect.objectContaining({
        favorites,
        search,
      }),
    );
  });

  it('exports rootSaga generator', () => {
    expect(isGeneratorFunction(rootSaga)).toBe(true);
  });

  // TODO:
  // it.skip('rootSaga initializes sagas', () => {});
  // it.skip('initializeSaga invokes saga', () => {});
  // it.skip('initializeSaga throws error', () => {});
});
