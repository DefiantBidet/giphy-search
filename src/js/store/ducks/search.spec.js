import { runSaga } from 'redux-saga';
import * as API from 'api';

import reducer, {
  actions,
  onUpdateSearchString,
  onSearchRequest,
  onSearchRequestLoaded,
  onSearchRequestError,
  onPageChange,
  onChangeLoading,
  sagas,
  searchRequestWorker,
} from './search';

const mockInitialState = {
  searchString: '',
  searchResults: [],
  isLoading: false,
  pagination: {
    currentPage: null,
    perPage: null,
    totalPages: null,
  },
};

// actions
describe('search store actions', () => {
  it('should export ON_UPDATE_SEARCH_STRING action label', () => {
    expect(actions.ON_UPDATE_SEARCH_STRING).toBeDefined();
    expect(actions.ON_UPDATE_SEARCH_STRING).toEqual('search/ON_UPDATE_SEARCH_STRING');
  });

  it('should export ON_SEARCH_REQUEST action label', () => {
    expect(actions.ON_SEARCH_REQUEST).toBeDefined();
    expect(actions.ON_SEARCH_REQUEST).toEqual('search/ON_SEARCH_REQUEST');
  });

  it('should export ON_SEARCH_REQUEST_LOADED action label', () => {
    expect(actions.ON_SEARCH_REQUEST_LOADED).toBeDefined();
    expect(actions.ON_SEARCH_REQUEST_LOADED).toEqual('search/ON_SEARCH_REQUEST_LOADED');
  });

  it('should export ON_SEARCH_REQUEST_ERROR action label', () => {
    expect(actions.ON_SEARCH_REQUEST_ERROR).toBeDefined();
    expect(actions.ON_SEARCH_REQUEST_ERROR).toEqual('search/ON_SEARCH_REQUEST_ERROR');
  });

  it('should export ON_PAGE_CHANGE action label', () => {
    expect(actions.ON_PAGE_CHANGE).toBeDefined();
    expect(actions.ON_PAGE_CHANGE).toEqual('search/ON_PAGE_CHANGE');
  });

  it('should export ON_LOADING_CHANGE action label', () => {
    expect(actions.ON_LOADING_CHANGE).toBeDefined();
    expect(actions.ON_LOADING_CHANGE).toEqual('search/ON_LOADING_CHANGE');
  });
});

// reducer
describe('search store reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(mockInitialState);
  });

  it('ON_UPDATE_SEARCH_STRING updates state with new searchString', () => {
    const mockQuery = 'foo';

    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_UPDATE_SEARCH_STRING,
        data: mockQuery,
      },
    );

    // deep copy of initial state
    const expectedState = JSON.parse(JSON.stringify(mockInitialState));
    expectedState.searchString = mockQuery;

    expect(state).toEqual(expectedState);
  });

  it('ON_SEARCH_REQUEST updates state to toggle loading', () => {
    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_SEARCH_REQUEST,
      },
    );

    // deep copy of initial state
    const expectedState = JSON.parse(JSON.stringify(mockInitialState));
    expectedState.isLoading = true;
    expectedState.pagination.currentPage = 0;
    expectedState.pagination.perPage = 20;

    expect(state).toEqual(expectedState);
  });

  it('ON_SEARCH_REQUEST_LOADED updates state with results', () => {
    const mockResults = [
      {
        id: '1',
        slug: 'foo',
        url: '/foo',
      },
      {
        id: '2',
        slug: 'foo-bar',
        url: '/foo-bar',
      },
      {
        id: '3',
        slug: 'foo-bar-baz',
        url: '/foo-bar-bar',
      },
    ];

    const mockPagesTotal = 375;
    const perPage = 20;

    const response = {
      results: mockResults,
      pagination: {
        total_count: mockPagesTotal,
        count: perPage,
        offset: 0,
      },
    };

    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_SEARCH_REQUEST_LOADED,
        data: response,
      },
    );

    const expectedState = JSON.parse(JSON.stringify(mockInitialState));
    expectedState.searchResults = mockResults;
    expectedState.pagination = {
      currentPage: 1,
      totalPages: Math.ceil(mockPagesTotal / perPage),
      perPage,
    };

    expect(state).toEqual(expectedState);
  });

  it('ON_SEARCH_REQUEST_ERROR updates state to stop loading', () => {
    const initialState = JSON.parse(JSON.stringify(mockInitialState));
    initialState.isLoading = true;

    const expectedState = JSON.parse(JSON.stringify(mockInitialState));
    expectedState.isLoading = false;

    const state = reducer(
      initialState,
      {
        type: actions.ON_SEARCH_REQUEST_ERROR,
        error: 'foo bar baz',
      },
    );

    expect(state).toEqual(expectedState);
  });

  it('ON_LOADING_CHANGE sets `isLoading` flag with supplied value', () => {
    // random boolean - 50/50 probability
    const mockBoolean = Math.random() >= 0.5;
    const expectedState = { ...mockInitialState };
    expectedState.isLoading = mockBoolean;

    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_LOADING_CHANGE,
        data: mockBoolean,
      },
    );

    expect(state).toEqual(expectedState);
  });

  it('ON_PAGE_CHANGE updates pagination data', () => {
    const page = 5;
    const totalPages = 25;

    const expectedState = JSON.parse(JSON.stringify(mockInitialState));
    expectedState.isLoading = true;
    expectedState.pagination = {
      currentPage: page,
      perPage: 20,
      totalPages,
    };

    const initialState = JSON.parse(JSON.stringify(mockInitialState));
    initialState.pagination = {
      currentPage: 1,
      perPage: 20,
      totalPages,
    };

    const state = reducer(
      initialState,
      {
        type: actions.ON_PAGE_CHANGE,
        data: page,
      },
    );
    expect(state).toEqual(expectedState);
  });
});

// action creators
describe('search store action creators', () => {
  it('should create onUpdateSearchString action', () => {
    const mockQueryString = 'foo';
    const mockAction = onUpdateSearchString(mockQueryString);
    const expectedAction = {
      type: actions.ON_UPDATE_SEARCH_STRING,
      data: mockQueryString,
    };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onSearchRequest action', () => {
    const mockQueryString = 'foo';
    const mockAction = onSearchRequest();
    const expectedAction = {
      type: actions.ON_SEARCH_REQUEST
    };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onSearchRequestLoaded action', () => {
    const mockResults = [
      {
        id: '1',
        slug: 'foo-bar',
        url: '/foo',
      },
    ];
    const mockAction = onSearchRequestLoaded(mockResults);
    const expectedAction = {
      type: actions.ON_SEARCH_REQUEST_LOADED,
      data: mockResults,
    };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onSearchRequestError action', () => {
    const mockError = new Error('foo');
    const mockAction = onSearchRequestError(mockError);
    const expectedAction = {
      type: actions.ON_SEARCH_REQUEST_ERROR,
      error: mockError,
    };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onPageChange action', () => {
    const mockPage = 10;
    const mockAction = onPageChange(mockPage);
    const expectedAction = {
      type: actions.ON_PAGE_CHANGE,
      data: mockPage,
    };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onChangeLoading action', () => {
    // random boolean - 50/50 probability
    const mockBoolean = Math.random() >= 0.5;
    const mockAction = onChangeLoading(mockBoolean);
    const expectedAction = {
      type: actions.ON_LOADING_CHANGE,
      data: mockBoolean,
    };

    expect(mockAction).toEqual(expectedAction);
  });
});

// sagas
describe('search store sagas', () => {
  describe('searchRequest saga', () => {
    it('should call API and dispatch request loaded action', async () => {
      const dispatched = [];
      const mockString = 'foo';
      const mockActionResponse = [
        {
          id: '1',
          title: 'test 1',
          slug: 'foo',
          url: '/foo-bar',
          height: '200',
          width: '200',
        },
        {
          id: '2',
          title: 'test 2',
          slug: 'bar',
          url: '/bar-baz',
          height: '200',
          width: '200',
        },
        {
          id: '3',
          title: 'test 3',
          slug: 'baz',
          url: '/spoon',
          height: '200',
          width: '200',
        },
      ];

      const mockApiResponseList = [
        {
          id: '1',
          title: 'test 1',
          slug: 'foo',
          images: {
            original: {
              url: '/foo-bar',
              height: '200',
              width: '200',
            },
          },
        },
        {
          id: '2',
          title: 'test 2',
          slug: 'bar',
          images: {
            original: {
              url: '/bar-baz',
              height: '200',
              width: '200',
            },
          },
        },
        {
          id: '3',
          title: 'test 3',
          slug: 'baz',
          images: {
            original: {
              url: '/spoon',
              height: '200',
              width: '200',
            },
          },
        },
      ];

      const mockResults = {
        data: mockApiResponseList,
        pagination: {
          total_count: 200,
          count: 20,
          offset: 0,
        },
        meta: {
          status: 200,
          msg: 'OK',
          response_id: '12345',
        },
      };

      const mockResponse = {
        data: mockResults,
        status: 200,
        statusText: '',
        headers: {},
        config: {},
        request: {},
      };

      const mockActionData = {
        results: mockActionResponse,
        pagination: mockResults.pagination,
      };

      const initialState = JSON.parse(JSON.stringify(mockInitialState));
      initialState.searchString = mockString;

      const mockFetch = jest.spyOn(API, 'default')
        .mockImplementation(() => Promise.resolve(mockResponse));

      await runSaga({
        dispatch: (action) => dispatched.push(action),
        getState: () => ({
          search: initialState,
        }),
      }, searchRequestWorker, {
        searchString: mockString,
        pageOffset: 0,
      });

      expect(mockFetch).toHaveBeenCalledTimes(1);
      expect(dispatched).toEqual([onSearchRequestLoaded(mockActionData)]);

      mockFetch.mockRestore();
    });

    it('should call API and dispatch request error action', async () => {
      const dispatched = [];
      const mockString = 'foo';

      const initialState = JSON.parse(JSON.stringify(mockInitialState));
      initialState.searchString = mockString;

      const mockFetch = jest.spyOn(API, 'default')
        .mockImplementation(() => Promise.reject());

      await runSaga({
        dispatch: (action) => dispatched.push(action),
        getState: () => ({
          search: initialState,
        }),
      }, searchRequestWorker, {
        searchString: mockString,
        pageOffset: 0,
      });

      expect(mockFetch).toHaveBeenCalledTimes(1);
      expect(dispatched).toEqual([onSearchRequestError()]);

      mockFetch.mockRestore();
    });
  });
});
