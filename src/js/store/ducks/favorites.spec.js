import reducer, {
  actions,
  onSaveFavorite,
  onRemoveFavorite,
  onShowFavorites,
  onPageChange,
} from './favorites';

const mockInitialState = {
  showFavorites: false,
  saved: [],
  pagination: {
    currentPage: null,
    perPage: null,
    totalPages: null,
  },
};

// actions
describe('favorites store actions', () => {
  it('should export ON_SAVE_FAVORITE action label', () => {
    expect(actions.ON_SAVE_FAVORITE).toBeDefined();
    expect(actions.ON_SAVE_FAVORITE).toEqual('favorites/ON_SAVE_GIF');
  });

  it('should export ON_REMOVE_FAVORITE action label', () => {
    expect(actions.ON_REMOVE_FAVORITE).toBeDefined();
    expect(actions.ON_REMOVE_FAVORITE).toEqual('favorites/ON_REMOVE_GIF');
  });

  it('should export ON_SHOW_FAVORITES action label', () => {
    expect(actions.ON_SHOW_FAVORITES).toBeDefined();
    expect(actions.ON_SHOW_FAVORITES).toEqual('favorites/ON_SHOW_FAVORITES');
  });

  it('should export ON_PAGE_CHANGE action label', () => {
    expect(actions.ON_PAGE_CHANGE).toBeDefined();
    expect(actions.ON_PAGE_CHANGE).toEqual('favorites/ON_PAGE_CHANGE');
  });
});

// reducer
describe('favorites store reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(mockInitialState);
  });

  it('ON_SAVE_FAVORITE should add to saved list', () => {
    const mockFavorite = {
      id: '1',
      slug: 'foo-bar',
      url: '/foo',
      created_at: Date.now(),
    };

    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_SAVE_FAVORITE,
        data: mockFavorite,
      },
    );

    const expectedState = {
      ...mockInitialState,
      saved: [mockFavorite],
    };

    expectedState.pagination.currentPage = 0;
    expectedState.pagination.perPage = 20;
    expectedState.pagination.totalPages = 1;

    expect(state).toEqual(expectedState);
  });

  it('ON_SAVE_FAVORITE should add to existing saved list', () => {
    const mockFav1 = {
      id: '1',
      slug: 'foo',
      url: '/foo',
      created_at: Date.now(),
    };

    const mockFav2 = {
      id: '2',
      slug: 'foo-bar',
      url: '/foo-bar',
      created_at: Date.now(),
    };

    const initialStateMock = {
      ...mockInitialState,
      saved: [mockFav1],
    };

    const state = reducer(
      initialStateMock,
      {
        type: actions.ON_SAVE_FAVORITE,
        data: mockFav2,
      },
    );

    const expectedState = {
      ...initialStateMock,
      saved: [
        mockFav1,
        mockFav2,
      ],
    };

    expectedState.pagination.currentPage = 0;
    expectedState.pagination.perPage = 20;
    expectedState.pagination.totalPages = 1;

    expect(state).toEqual(expectedState);
  });

  it('ON_REMOVE_FAVORITE should remove from saved list', () => {
    const mockFav1 = {
      id: '1',
      slug: 'foo',
      url: '/foo',
      created_at: Date.now(),
    };
    const mockFav2 = {
      id: '2',
      slug: 'foo-bar',
      url: '/foo-bar',
      created_at: Date.now(),
    };
    const initialStateMock = {
      saved: [
        mockFav1,
        mockFav2,
      ],
    };

    const state = reducer(
      initialStateMock,
      {
        type: actions.ON_REMOVE_FAVORITE,
        data: '2',
      },
    );

    const expectedState = {
      saved: [mockFav1],
    };

    expect(state).toEqual(expectedState);
  });

  it('ON_REMOVE_FAVORITE should not remove from existing saved list if id is not found', () => {
    const mockFav1 = {
      id: '1',
      slug: 'foo',
      url: '/foo',
      created_at: Date.now(),
    };
    const mockFav2 = {
      id: '2',
      slug: 'foo-bar',
      url: '/foo-bar',
      created_at: Date.now(),
    };
    const initialStateMock = {
      saved: [
        mockFav1,
        mockFav2,
      ],
    };

    const state = reducer(
      initialStateMock,
      {
        type: actions.ON_REMOVE_FAVORITE,
        data: new Date().valueOf(),
      },
    );

    expect(state).toEqual(initialStateMock);
  });

  it('ON_SHOW_FAVORITES toggles `showFavorites` flag', () => {
    const expectedState = { ...mockInitialState };
    expectedState.showFavorites = !expectedState.showFavorites;

    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_SHOW_FAVORITES,
      },
    );

    expect(state).toEqual(expectedState);
  });

  it('ON_PAGE_CHANGE updates pagination data', () => {
    const page = 5;

    const expectedState = {
      ...mockInitialState,
      isLoading: true,
      pagination: {
        currentPage: page,
        perPage: 20,
        totalPages: 1,
      },
    };

    const state = reducer(
      mockInitialState,
      {
        type: actions.ON_PAGE_CHANGE,
        data: page,
      },
    );
    expect(state).toEqual(expectedState);
  });
});

// action creators
describe('favorites store action creators', () => {
  let nowMock;

  beforeAll(() => {
    // spy on Date.now() and return mock
    nowMock = jest.spyOn(global.Date, 'now')
      .mockImplementation(() => 1234567890);
  });

  afterAll(() => {
    nowMock.mockRestore();
  });

  it('should create onSaveFavorite action', () => {
    const mockFavoriteObject = {
      id: '1',
      slug: 'foo-bar',
      url: '/foo',
    };

    const mockAction = onSaveFavorite(mockFavoriteObject);
    const expectedAction = {
      type: actions.ON_SAVE_FAVORITE,
      data: {
        id: mockFavoriteObject.id,
        slug: mockFavoriteObject.slug,
        url: mockFavoriteObject.url,
        created_at: Date.now(),
      },
    };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onRemoveFavorite action', () => {
    const mockId = 'e95a9d07-0b24-4470-b37d-914c729585f2';
    const expectedAction = {
      type: actions.ON_REMOVE_FAVORITE,
      data: mockId,
    };

    expect(onRemoveFavorite(mockId)).toEqual(expectedAction);
  });

  it('should create onShowFavorites action', () => {
    const mockAction = onShowFavorites();
    const expectedAction = { type: actions.ON_SHOW_FAVORITES };

    expect(mockAction).toEqual(expectedAction);
  });

  it('should create onPageChange action', () => {
    const mockPage = 10;
    const mockAction = onPageChange(mockPage);
    const expectedAction = {
      type: actions.ON_PAGE_CHANGE,
      data: mockPage,
    };

    expect(mockAction).toEqual(expectedAction);
  });
});
