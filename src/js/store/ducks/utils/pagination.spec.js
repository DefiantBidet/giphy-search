import reducer, {
  createPaginationObject,
  calculatePagination,
  calculatePageOffset,
  calculateFavoritePagination,
  calculateFavoriteOffset,
  resetPagination,
} from './pagination';

const paginationDefaults = {
  currentPage: null,
  perPage: null,
  totalPages: null,
};

describe('ducks pagination utilities', () => {
  it('createPaginationObject returns initial pagination object', () => {
    const expected = paginationDefaults;
    const pagination = createPaginationObject();

    expect(pagination).toEqual(expected);
  });

  it('calculatePagination converts the pagination schema of the API', () => {
    const mockApiPagination = {
      total_count: 100,
      count: 20,
      offset: 0,
    };

    const expected = JSON.parse(JSON.stringify(paginationDefaults));
    expected.currentPage = 1;
    expected.perPage = 20;
    expected.totalPages = Math.ceil(mockApiPagination.total_count / expected.perPage);

    const pagination = calculatePagination(mockApiPagination);

    expect(pagination).toEqual(expected);
  });

  it('calculatePageOffset calculates the offset of the pages as per the API', () => {
    const mockPage = 5;
    const mockPerPage = 20; // this is a pagination constant
    const expected = mockPage * mockPerPage;

    const paginationObject = JSON.parse(JSON.stringify(paginationDefaults));
    paginationObject.currentPage = mockPage;

    const offset = calculatePageOffset(paginationObject);

    expect(offset).toEqual(expected);
  });

  it('calculateFavoritePagination calculates pagination for favorites', () => {
    const mockList = [1, 2, 3, 4, 5];

    const expected = JSON.parse(JSON.stringify(paginationDefaults));
    expected.currentPage = 0;
    expected.perPage = 20;
    expected.totalPages = Math.ceil(mockList.length / expected.perPage);

    const pagination = calculateFavoritePagination(mockList);

    expect(pagination).toEqual(expected);
  });

  it('resetPagination resets to default pagination object', () => {
    const expected = JSON.parse(JSON.stringify(paginationDefaults));
    expected.currentPage = 0;
    expected.perPage = 20;

    const pagination = resetPagination();

    expect(pagination).toEqual(expected);
  });
});
