// using default value via API - this would need to
// be stored as a config value if we wanted to make this user configurable
const perPage = 20;

function createPaginationObject() {
  return {
    currentPage: null,
    perPage: null,
    totalPages: null,
  };
}

function calculatePagination(pagination) {
  const { total_count: totalCount, offset } = pagination;
  const totalPages = Math.ceil(totalCount / perPage);
  const pageOffset = offset;

  // page is going to be one based
  // offset divided / per page as per GIPHY API
  // add one to align zero based offset with one based page count
  const currentPage = (pageOffset / perPage) + 1;

  return {
    currentPage,
    perPage,
    totalPages,
  };
}

function calculatePageOffset(pagination) {
  const { currentPage } = pagination;
  const pageOffset = currentPage * perPage;
  return pageOffset;
}

function calculateFavoritePagination(list) {
  const totalPages = Math.ceil(list.length / perPage);
  const currentPage = 0;

  return {
    currentPage,
    perPage,
    totalPages,
  };
}

function resetPagination() {
  return {
    perPage,
    currentPage: 0,
    totalPages: null,
  };
}

export {
  createPaginationObject,
  calculatePagination,
  calculatePageOffset,
  calculateFavoritePagination,
  resetPagination,
};
