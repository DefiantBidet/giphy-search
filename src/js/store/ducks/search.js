import {
  call,
  put,
  select,
  takeLatest,
} from 'redux-saga/effects';

import fetchGIFs from 'api';

import {
  createPaginationObject,
  calculatePagination,
  calculatePageOffset,
  resetPagination,
} from './utils/pagination';

/*
********************************************************************************
**                               Initial State                                **
********************************************************************************
*/
const INITIAL_STATE = {
  searchString: '',
  searchResults: [],
  isLoading: false,
  pagination: createPaginationObject(),
};

/*
********************************************************************************
**                               Helper Methos                                **
********************************************************************************
*/
function gifModel(data) {
  const gif = {
    id: data.id,
    title: data.title,
    slug: data.slug,
    url: data.images.original.url,
    height: data.images.original.height,
    width: data.images.original.width,
  };
  return gif;
}
/*
********************************************************************************
**                                   Actions                                  **
********************************************************************************
*/
const ON_UPDATE_SEARCH_STRING = 'search/ON_UPDATE_SEARCH_STRING';
const ON_SEARCH_REQUEST = 'search/ON_SEARCH_REQUEST';
const ON_SEARCH_REQUEST_LOADED = 'search/ON_SEARCH_REQUEST_LOADED';
const ON_SEARCH_REQUEST_ERROR = 'search/ON_SEARCH_REQUEST_ERROR';
const ON_PAGE_CHANGE = 'search/ON_PAGE_CHANGE';
const ON_LOADING_CHANGE = 'search/ON_LOADING_CHANGE';

const actions = {
  ON_UPDATE_SEARCH_STRING,
  ON_SEARCH_REQUEST,
  ON_SEARCH_REQUEST_LOADED,
  ON_SEARCH_REQUEST_ERROR,
  ON_PAGE_CHANGE,
  ON_LOADING_CHANGE,
};

/*
********************************************************************************
**                                   Redcuer                                  **
********************************************************************************
*/
function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case ON_UPDATE_SEARCH_STRING: {
      const searchString = action.data;

      return {
        ...state,
        searchString,
      };
    }

    case ON_SEARCH_REQUEST: {
      const isLoading = true;

      const pagination = resetPagination();

      return {
        ...state,
        isLoading,
        pagination,
      };
    }

    case ON_SEARCH_REQUEST_LOADED: {
      const response = action.data;
      const pagination = calculatePagination(response.pagination);

      return {
        ...state,
        pagination,
        searchResults: response.results,
      };
    }

    case ON_SEARCH_REQUEST_ERROR: {
      const isLoading = false;
      return {
        ...state,
        isLoading,
      };
    }

    case ON_PAGE_CHANGE: {
      const { pagination: statePagination } = state;
      // deep clone of state object for immutability
      const pagination = JSON.parse(JSON.stringify(statePagination));
      pagination.currentPage = action.data;

      const isLoading = true;

      return {
        ...state,
        isLoading,
        pagination,
      };
    }

    case ON_LOADING_CHANGE: {
      const isLoading = action.data;

      return {
        ...state,
        isLoading,
      };
    }

    default: return state;
  }
}

/*
********************************************************************************
**                               Action Creators                              **
********************************************************************************
*/
function onUpdateSearchString(text) {
  return {
    type: ON_UPDATE_SEARCH_STRING,
    data: text,
  };
}

function onSearchRequest() {
  return {
    type: ON_SEARCH_REQUEST,
  };
}

function onSearchRequestLoaded(results) {
  return {
    type: ON_SEARCH_REQUEST_LOADED,
    data: results,
  };
}

function onSearchRequestError(error) {
  return {
    type: ON_SEARCH_REQUEST_ERROR,
    error,
  };
}

function onPageChange(page) {
  return {
    type: ON_PAGE_CHANGE,
    data: page,
  };
}

function onChangeLoading(isLoading) {
  return {
    type: ON_LOADING_CHANGE,
    data: isLoading,
  };
}

/*
********************************************************************************
**                                    Sagas                                   **
********************************************************************************
*/
function* searchRequestWorker() {
  try {
    const state = yield select();

    // FIXME:
    // if (!state.search.isLoading) {
    //   yield put(onSearchRequestCancelled());
    //   return;
    // }

    const { searchString, pagination: statePagination } = state.search;
    const queryObject = {
      searchString,
      pageOffset: calculatePageOffset(statePagination),
    };
    const response = yield call(fetchGIFs, queryObject);

    // axios' response.data
    const { data } = response;
    // api schema
    const { data: resultList, pagination: responsePagination } = data;
    const searchResults = resultList.map((gifObject) => gifModel(gifObject));

    const actionData = {
      results: searchResults,
      pagination: responsePagination,
    };

    yield put(onSearchRequestLoaded(actionData));
  } catch (error) {
    yield put(onSearchRequestError(error));
  }
}

function* searchRequestWatcher() {
  yield takeLatest([ON_SEARCH_REQUEST, ON_PAGE_CHANGE], searchRequestWorker);
}

const sagas = [searchRequestWatcher];

/*
********************************************************************************
**                                  Exports                                   **
********************************************************************************
*/

export {
  actions,
  onUpdateSearchString,
  onSearchRequest,
  onSearchRequestLoaded,
  onSearchRequestError,
  onPageChange,
  onChangeLoading,
  sagas,
  searchRequestWorker,
};

export default reducer;
