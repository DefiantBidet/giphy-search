import store from '.';

describe('Redux Data Store', () => {
  it('creates a redux store', () => {
    expect(store).toEqual(
      expect.objectContaining({
        getState: expect.any(Function),
        dispatch: expect.any(Function),
        subscribe: expect.any(Function),
        replaceReducer: expect.any(Function),
      }),
    );
  });
});
