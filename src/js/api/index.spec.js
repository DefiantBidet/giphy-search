import axios from 'axios';
import fetchGIFs from '.';

jest.mock('axios');

describe('API :: fetchGIFs', () => {
  it('successfully fetches data from API', async () => {
    const mockQueryObject = {
      searchString: 'foo',
      pageOffset: 0,
    };

    const mockResponse = {
      data: {
        list: [
          {
            id: 0,
            title: 'foo',
          },
          {
            id: 1,
            title: 'bar',
          },
        ],
      },
    };

    axios.mockImplementationOnce(() => Promise.resolve(mockResponse));

    await expect(fetchGIFs(mockQueryObject)).resolves.toEqual(mockResponse);
  });

  it('fetches erroneously data from an API', async () => {
    const errorMessage = 'Network Error';
    axios.mockImplementationOnce(() => Promise.reject(new Error(errorMessage)));

    const mockQueryObject = {
      searchString: 'foo',
      pageOffset: 0,
    };

    await expect(fetchGIFs(mockQueryObject)).rejects.toThrow(errorMessage);
  });
});
