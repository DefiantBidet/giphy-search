import axios from 'axios';

async function fetchGIFs(queryObject) {
  const { searchString, pageOffset } = queryObject;

  const options = {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
    },
    params: {
      api_key: 'GZKGwdu6xlIM0iV58yFKJOFLqj0NLXFw',
      q: searchString,
      offset: pageOffset,
      limit: 15,
      rating: 'PG',
    },
    url: 'https://api.giphy.com/v1/gifs/search',
  };

  return axios(options);
}

export default fetchGIFs;
