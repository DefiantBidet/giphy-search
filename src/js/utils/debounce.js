export default function debounce(fn, time) {
  let timeout;

  return function innerDebounce(...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => fn.apply(this, args), time);
  };
}
