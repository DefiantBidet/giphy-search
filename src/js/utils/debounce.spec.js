import debounce from './debounce';

// tell jest to mock all timeout functions
jest.useFakeTimers();

describe('debounce', () => {
  it('should call debounced function after interval', () => {
    const func = jest.fn();
    const debouncedFunc = debounce(func, 1000);

    // call function
    debouncedFunc();
    expect(func).toBeCalledTimes(0);

    // fast-forward time
    jest.runAllTimers();
    expect(func).toBeCalledTimes(1);
  });

  it('should call debounced function only once', () => {
    const func = jest.fn();
    const debouncedFunc = debounce(func, 1000);

    // call function several times
    let i = 0;
    while (i < 100) {
      debouncedFunc();
      i += 1;
    }

    // fast-forward time
    jest.runAllTimers();
    expect(func).toBeCalledTimes(1);
  });
});
