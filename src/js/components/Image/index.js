import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from 'components/image.scss';

const initialState = {
  imageSpans: 0,
  isLoaded: false,
  isMouseOver: false,
};

class Image extends Component {
  constructor(props) {
    super(props);

    // height for image span calculation
    this.imageSpan = 25;

    // Ref attribute on the DOM image element
    this.imageRef = React.createRef();

    this.onImageLoaded = this.onImageLoaded.bind(this);
    this.onImageRollOver = this.onImageRollOver.bind(this);
    this.onImageRollOut = this.onImageRollOut.bind(this);
    this.onImageClick = this.onImageClick.bind(this);

    this.state = initialState;
  }

  componentDidMount() {
    if (this.state.imageSpans !== 0) {
      this.props.onLoaded(this.props.imageId);
    }
  }

  onImageLoaded() {
    // masonry style image layout
    const height = this.imageRef.current.clientHeight;
    const imageSpans = Math.ceil(height / this.imageSpan);

    this.props.onLoaded(this.props.imageId);

    this.setState({
      imageSpans,
      isLoaded: true,
    });
  }

  onImageRollOver(event) {
    event.preventDefault();
    event.stopPropagation();

    this.setState({ isMouseOver: true });
  }

  onImageRollOut(event) {
    event.preventDefault();
    event.stopPropagation();

    this.setState({ isMouseOver: false });
  }

  onImageClick(event) {
    event.preventDefault();
    event.stopPropagation();

    const { onClick, imageId } = this.props;

    onClick(imageId);
  }

  renderFavoriteOverlay() {
    const { isFavorite } = this.props;
    const { isMouseOver } = this.state;

    const ariaLabel = isFavorite ? 'Remove From Favorites' : 'Add To Favorites';
    const classList = isMouseOver || isFavorite
      ? `${styles.favoriteOVerlay} ${styles.slideIn}`
      : `${styles.favoriteOVerlay} ${styles.slideOut}`;

    const icon = isFavorite ? '#svg-icon_favorite--filled' : '#svg-icon_favorite';

    return (
      <div className={classList}>
        <svg className='img' role='img' aria-label={ariaLabel}>
          <use xlinkHref={icon}></use>
        </svg>
      </div>
    );
  }

  render() {
    const { imageUrl, isFavorite } = this.props;

    // don't add over/out handlers if already a favorite
    // intent for the banner to act as a notification of favorite
    const overlayAttributes = {
      onMouseOver: isFavorite ? null : this.onImageRollOver,
      onMouseOut: isFavorite ? null : this.onImageRollOut,
    };

    // hack for cached images not sending onload event
    if (this.state.imageSpans !== 0) {
      this.props.onLoaded(this.props.imageId);
    }

    return (
      <div
        {...overlayAttributes}
        role='button'
        className={styles.imageContainer}
        onClick={this.onImageClick}
        style={{ gridRowEnd: `span ${this.state.imageSpans}` }}
      >
        <img
          className={styles.image}
          ref={this.imageRef}
          src={imageUrl}
          onLoad={this.onImageLoaded}
        />
        {this.renderFavoriteOverlay()}
      </div>
    );
  }
}

Image.propTypes = {
  imageId: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool,
  onClick: PropTypes.func,
  onLoaded: PropTypes.func,
};

Image.defaultProps = {
  isFavorite: false,
  onClick: () => { /* no-op */ },
  onLoaded: () => { /* no-op */ },
};

export default Image;
