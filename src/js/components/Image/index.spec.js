import React from 'react';
import { mount } from 'enzyme';

import Image from '.';

const mockProps = {
  imageId: '807dbea2-6885-4887-91b1-7dbb81d8ee32',
  imageUrl: '/foo-bar',
};

describe('<Image />', () => {
  describe('snapshots', () => {
    it('matches snapshot - unfavorite', () => {
      const wrapper = mount(<Image {...mockProps} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches snapshot - favorite', () => {
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.isFavorite = true;

      const wrapper = mount(<Image {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('DOM structure', () => {
    it('creates an div container', () => {
      const wrapper = mount(<Image {...mockProps} />);
      const imageContainer = wrapper.childAt(0);
      const containerDomNode = imageContainer.getDOMNode();

      expect(imageContainer.type()).toBe('div');
      expect(containerDomNode.getAttribute('role')).toBe('button');
    });

    it('creates an image element', () => {
      const wrapper = mount(<Image {...mockProps} />);
      const imageContainer = wrapper.childAt(0);
      const image = imageContainer.childAt(0);

      expect(image.type()).toBe('img');
    });

    it('creates a favorite overlay', () => {
      const wrapper = mount(<Image {...mockProps} />);
      const imageContainer = wrapper.childAt(0);
      const overlay = imageContainer.childAt(1);

      expect(overlay.type()).toBe('div');
    });

    it('overlay has svg icon', () => {
      const wrapper = mount(<Image {...mockProps} />);
      const imageContainer = wrapper.childAt(0);
      const overlay = imageContainer.childAt(1);
      const svg = overlay.childAt(0);
      const svgDomNode = svg.getDOMNode();

      expect(svg.type()).toBe('svg');
      expect(svgDomNode.getAttribute('role')).toBe('img');
    });

    it('overlay icon has appropriate label if favorite', () => {
      const mockIsFavorite = true;
      const mockLabel = 'Remove From Favorites';

      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.isFavorite = mockIsFavorite;

      const wrapper = mount(<Image {...propsMock} />);
      const svg = wrapper.find('svg.img');
      const svgDomNode = svg.getDOMNode();

      expect(svgDomNode.getAttribute('aria-label')).toBe(mockLabel);
    });

    it('overlay icon has appropriate label if not favorite', () => {
      const mockIsFavorite = false;
      const mockLabel = 'Add To Favorites';

      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.isFavorite = mockIsFavorite;

      const wrapper = mount(<Image {...propsMock} />);
      const svg = wrapper.find('svg.img');
      const svgDomNode = svg.getDOMNode();

      expect(svgDomNode.getAttribute('aria-label')).toBe(mockLabel);
    });
  });

  describe('interactions', () => {
    it('handles roll over if not favorite', () => {
      const spy = jest.spyOn(Image.prototype, 'onImageRollOver');
      const wrapper = mount(<Image {...mockProps} />);
      const imageContainer = wrapper.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);
      imageContainer.simulate('mouseover');
      expect(spy).toHaveBeenCalledTimes(1);
      spy.mockRestore();
    });

    it('handles roll out if not favorite', () => {
      const spy = jest.spyOn(Image.prototype, 'onImageRollOut');
      const wrapper = mount(<Image {...mockProps} />);
      const imageContainer = wrapper.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);
      imageContainer.simulate('mouseout');
      expect(spy).toHaveBeenCalledTimes(1);
      spy.mockRestore();
    });

    it('ignores roll over if favorite', () => {
      const spy = jest.spyOn(Image.prototype, 'onImageRollOver');

      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.isFavorite = true;

      const wrapper = mount(<Image {...propsMock} />);
      const imageContainer = wrapper.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);
      imageContainer.simulate('mouseover');
      expect(spy).toHaveBeenCalledTimes(0);
      spy.mockRestore();
    });

    it('ignores roll out if favorite', () => {
      const spy = jest.spyOn(Image.prototype, 'onImageRollOver');

      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.isFavorite = true;

      const wrapper = mount(<Image {...propsMock} />);
      const imageContainer = wrapper.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);
      imageContainer.simulate('mouseout');
      expect(spy).toHaveBeenCalledTimes(0);
      spy.mockRestore();
    });

    it('calls supplied click handler onClick', () => {
      const spy = jest.fn();
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.onClick = spy;

      const wrapper = mount(<Image {...propsMock} />);
      const imageContainer = wrapper.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);

      imageContainer.simulate('click', { target: { test: 0 } });
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
