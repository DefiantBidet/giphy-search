import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from 'components/searchButton.scss';

class SearchButton extends Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    event.stopPropagation();

    const { onClick } = this.props;

    onClick(event);
  }

  render() {
    const { ariaLabel, loading } = this.props;
    const className = loading ? styles.inactiveButton : styles.activeButton;
    const icon = loading ? 'loading' : 'search';
    const svgClass = loading ? `img ${styles.spin}` : 'img';

    return (
      <React.Fragment>
        <button
          type='button'
          className={className}
          onClick={this.onClick}
        >
          <svg className={svgClass} role='img' aria-label={ariaLabel}>
            <use xlinkHref={`#svg-icon_${icon}`}></use>
          </svg>
        </button>
      </React.Fragment>
    );
  }
}

SearchButton.propTypes = {
  ariaLabel: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
};

SearchButton.defaultProps = {
  onClick: () => { /* no-op */ },
};

export default SearchButton;
