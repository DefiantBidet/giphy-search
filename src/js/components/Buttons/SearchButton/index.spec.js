import React from 'react';
import { mount } from 'enzyme';

import SearchButton from '.';

const mockProps = {
  ariaLabel: 'foo',
  loading: false,
};

// onClick

describe('<SearchButton />', () => {
  describe('snapshots', () => {
    it('matches snapshot - not loading', () => {
      const wrapper = mount(<SearchButton {...mockProps} />);
      expect(wrapper).toMatchSnapshot();
    });

    it('matches snapshot - loading', () => {
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.loading = true;

      const wrapper = mount(<SearchButton {...propsMock} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('DOM structure', () => {
    it('creates a button', () => {
      const wrapper = mount(<SearchButton {...mockProps} />);
      const button = wrapper.childAt(0);
      const containerDomNode = button.getDOMNode();

      expect(button.type()).toBe('button');
      expect(containerDomNode.getAttribute('type')).toBe('button');
    });

    it('creates an svg', () => {
      const wrapper = mount(<SearchButton {...mockProps} />);
      const button = wrapper.childAt(0);
      const svg = button.childAt(0);

      expect(svg.type()).toBe('svg');
    });

    it('svg has role and `aria-label`', () => {
      const wrapper = mount(<SearchButton {...mockProps} />);
      const button = wrapper.childAt(0);
      const svg = button.childAt(0);
      const svgDomNode = svg.getDOMNode();

      expect(svgDomNode.getAttribute('aria-label')).toBe(mockProps.ariaLabel);
      expect(svgDomNode.getAttribute('role')).toBe('img');
    });

    it('svg renders search icon', () => {
      const mockIcon = '#svg-icon_search';
      const wrapper = mount(<SearchButton {...mockProps} />);
      const button = wrapper.childAt(0);
      const svg = button.childAt(0);
      const use = svg.childAt(0);
      const useDomNode = use.getDOMNode();

      expect(useDomNode.getAttributeNS('http://www.w3.org/1999/xlink', 'href'))
        .toBe(mockIcon);
    });

    it('svg renders loading icon', () => {
      const mockIcon = '#svg-icon_loading';

      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.loading = true;

      const wrapper = mount(<SearchButton {...propsMock} />);
      const button = wrapper.childAt(0);
      const svg = button.childAt(0);
      const use = svg.childAt(0);
      const useDomNode = use.getDOMNode();

      expect(useDomNode.getAttributeNS('http://www.w3.org/1999/xlink', 'href'))
        .toBe(mockIcon);
    });
  });

  describe('interactions', () => {
    it('calls supplied click handler onClick', () => {
      const spy = jest.fn();
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.onClick = spy;

      const wrapper = mount(<SearchButton {...propsMock} />);
      const button = wrapper.childAt(0);

      expect(spy).toHaveBeenCalledTimes(0);

      button.simulate('click', { target: { test: 0 } });
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
