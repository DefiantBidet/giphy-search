import React from 'react';
import { mount } from 'enzyme';

import TextInput from '.';

const mockProps = {
  elementName: 'foo',
  placeHolder: 'Lorem ipsum voluptate ea ad.',
};

describe('<TextInput />', () => {
  describe('snapshots', () => {
    it('matches snapshot', () => {
      const wrapper = mount(<TextInput {...mockProps} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('DOM structure', () => {
    it('creates input', () => {
      const wrapper = mount(<TextInput {...mockProps} />);
      expect(wrapper.children()).toHaveLength(1);
      expect(wrapper.childAt(0).type()).toBe('input');
    });

    it('creates input populated with supplied value', () => {
      const mockValue = 'fooooooo!';
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.inputValue = mockValue;

      const wrapper = mount(<TextInput {...propsMock} />);
      const inputEl = wrapper.find(`#${mockProps.elementName}`);

      expect(inputEl.getDOMNode().value).toEqual(mockValue);
    });

    it('creates input with `aria-label`', () => {
      const mockLabel = 'FOO';
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.label = mockLabel;

      const wrapper = mount(<TextInput {...propsMock} />);
      const inputEl = wrapper.find(`#${mockProps.elementName}`);

      expect(inputEl.getDOMNode().getAttribute('aria-label')).toEqual(mockLabel);
    });
  });

  describe('interactions', () => {
    it('calls supplied input handler onInput', () => {
      const mockInput = 'testing';
      const spy = jest.fn();
      const propsMock = JSON.parse(JSON.stringify(mockProps));
      propsMock.onInput = spy;

      const wrapper = mount(<TextInput {...propsMock} />);
      const inputEl = wrapper.find(`#${mockProps.elementName}`);

      expect(spy).toHaveBeenCalledTimes(0);

      inputEl.simulate('input', { target: { value: mockInput } });

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
