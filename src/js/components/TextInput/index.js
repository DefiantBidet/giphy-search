import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from 'components/textInput.scss';

class TextInput extends Component {
  constructor(props) {
    super(props);
    // Ref attribute on the DOM input element
    this.textInput = undefined;

    // Binding instance event handlers
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onInput = this.onInput.bind(this);
    this.createInputReference = this.createInputReference.bind(this);
  }

  onBlur(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  onFocus(e) {
    // calling stopPropagation in the capture phase prevents cursor display
    // @see https://bugzilla.mozilla.org/show_bug.cgi?id=509684
    // e.stopPropagation();
    e.preventDefault();
  }

  onInput(e) {
    e.stopPropagation();
    e.preventDefault();

    const { value } = e.target;
    const { onInput } = this.props;

    onInput(value);
  }

  createInputReference(el) {
    this.textInput = el;
  }

  createInput() {
    const {
      elementName,
      inputValue,
      label,
      placeHolder,
    } = this.props;

    const attributes = {
      'aria-required': true,
      'aria-label': label,
      id: elementName,
      className: styles.input,
      name: elementName,
      onBlur: this.onBlur,
      onFocus: this.onFocus,
      onInput: this.onInput,
      type: 'text',
      ref: this.createInputReference,
    };

    if (inputValue) {
      attributes.defaultValue = inputValue;
    } else {
      attributes.placeholder = placeHolder;
    }

    return (
      <input
        {...attributes}
      />
    );
  }

  render() {
    return this.createInput();
  }
}

TextInput.propTypes = {
  elementName: PropTypes.string.isRequired,
  inputValue: PropTypes.string,
  onInput: PropTypes.func,
  label: PropTypes.string,
  placeHolder: PropTypes.string,
};

TextInput.defaultProps = {
  // README: inputValue is the value of the input.
  // setting this value to `undefined` or `null` will force this to be
  // and uncontrolled component, not what we want- throws warnings in
  // console
  // @see: https://facebook.github.io/react/docs/forms.html#controlled-components
  inputValue: '',
  onInput: () => { /* no-op */ },
  label: '',
  placeHolder: '',
};

export default TextInput;
