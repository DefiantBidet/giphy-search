import 'regenerator-runtime/runtime';
import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import dataStore from 'store';

import Search from 'containers/Search';
import ImageGrid from 'containers/ImageGrid';
import Pagination from 'containers/Pagination';

function App() {
  return (
    <main className="mainContainer" role="main">
      <Search />
      <ImageGrid />
      <Pagination />
    </main>
  );
}

const appElement = document.querySelector('div#app');

ReactDOM.render(
  <Provider store={dataStore}>
    <App />
  </Provider>,
  appElement,
);

export default App;
