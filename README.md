# Giphy Search Code Challenge
Inline with the specifications of the take home assessment, this applicaion creates a user interface to search for GIFs using Giphy's public API.

## Installation
This project uses webpack to bundle assets and runs them locally via webpack-dev-server. Tests are run via Jest. Installing the necessary dependencies can be accomplished via npm or yarn.

```bash
# npm
npm install

# yarn
yarn
```

## Running Development Locally
The project uses Babel, Webpack, and a number of other front end resources to bundle the application source. Locally, the project is served via webpack-dev-server running on port 8888. Once installed:

- run start script
  ```bash
  # npm
  npm run start

  # yarn
  yarn run start
  ```

- Open <a href="http://localhost:8888/" target="_blank" rel="noopener">http://localhost:8888/</a> in your browser

## Running Tests
Tests are run using Jest and enzyme. There are a few scripts defined to run tests.
scripts:

- `test`
  - runs the test suite with the notifications flag enabled
- `test:coverage`
  - runs the test suite with the notifications and coverage flags enabled. This will report test coverage information
- `test:updateSnapshot`
  - runs the test suite with the updateSnapshot flag enabled. This will update snapshots
- `test:watch`
  - runs the test suite with the watch and notifications flag enabled. Note: `watch` requires a this folder to be in a git repository. This will run the tests on files that have changed since the last git commit.
- `test:watchAll`
  - runs the test suite with the watchAll and notifications flag enabled. This will watch for changes and run the suite/defined tests on change.

```bash
# npm
npm run test
npm run test:coverage
npm run test:updateSnapshot
npm run test:watch
npm run test:watchAll

# yarn
y run test
y run test:coverage
y run test:updateSnapshot
y run test:watch
y run test:watchAll
```

#### Solution Notes:
The following are areas of potential improvement regarding the implementation.

Hooks over Redux and Sagas - I haven't used hooks much and opted to favor familiarity over recent trends. I'm fairly confident the hooks approach would simplify a lot of the redux/state code while making it more maintainable/easier to work on.

Pagination was not part of the specification, however was a part of the API. I implemented a very rudimentary pagination solution using a 3rd party library. This can be expanded upon - as currently the favorites pagination is not functionally the same as the search pagination.

The loading overlay was a quick way for me to hide the ugliness of the UI placing the images in a masonry style layout while they load. This created a onload issue that required a bit of kludge to get around. This is not ideal

The layout of the image grid is using css-grids in an attempt to recreate a Masonry style layout.
