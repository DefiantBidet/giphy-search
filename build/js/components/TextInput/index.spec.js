import React from 'react';
import { mount } from 'enzyme';
import TextInput from '.';
var mockProps = {
  elementName: 'foo',
  placeHolder: 'Lorem ipsum voluptate ea ad.'
};
describe('<TextInput />', function () {
  test('creates a fieldset', function () {
    var wrapper = mount( /*#__PURE__*/React.createElement(TextInput, mockProps));
    expect(wrapper.find('fieldset')).toHaveLength(1);
  });
  test('creates input', function () {
    var wrapper = mount( /*#__PURE__*/React.createElement(TextInput, mockProps));
    var fieldset = wrapper.find('fieldset');
    expect(fieldset.children()).toHaveLength(2);
    expect(fieldset.childAt(0).type()).toBe('input');
  });
  test('creates placeholder label', function () {
    var wrapper = mount( /*#__PURE__*/React.createElement(TextInput, mockProps));
    var fieldset = wrapper.find('fieldset');
    expect(fieldset.children()).toHaveLength(2);
    expect(fieldset.childAt(1).type()).toBe('label');
    expect(fieldset.childAt(1).text()).toBe(mockProps.placeHolder);
  });
});