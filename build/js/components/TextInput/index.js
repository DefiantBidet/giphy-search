function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from 'components/textInput.scss';

var TextInput = /*#__PURE__*/function (_Component) {
  _inherits(TextInput, _Component);

  var _super = _createSuper(TextInput);

  function TextInput(props) {
    var _this;

    _classCallCheck(this, TextInput);

    _this = _super.call(this, props); // Ref attribute on the DOM input element

    _this.textInput = undefined; // Binding instance event handlers

    _this.onBlur = _this.onBlur.bind(_assertThisInitialized(_this));
    _this.onFocus = _this.onFocus.bind(_assertThisInitialized(_this));
    _this.onInput = _this.onInput.bind(_assertThisInitialized(_this));
    _this.createInputReference = _this.createInputReference.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(TextInput, [{
    key: "onBlur",
    value: function onBlur(e) {
      e.stopPropagation();
      e.preventDefault();
    }
  }, {
    key: "onFocus",
    value: function onFocus(e) {
      // calling stopPropagation in the capture phase prevents cursor display
      // @see https://bugzilla.mozilla.org/show_bug.cgi?id=509684
      // e.stopPropagation();
      e.preventDefault();
    }
  }, {
    key: "onInput",
    value: function onInput(e) {
      e.stopPropagation();
      e.preventDefault();
      var value = e.target.value;
      var onInput = this.props.onInput;
      onInput(value);
    }
  }, {
    key: "createInputReference",
    value: function createInputReference(el) {
      this.textInput = el;
    }
  }, {
    key: "createLabel",
    value: function createLabel() {
      var elementName = this.props.elementName;
      return /*#__PURE__*/React.createElement("label", {
        "aria-hidden": "true",
        className: styles.placeHolder,
        htmlFor: elementName
      }, "BPM");
    }
  }, {
    key: "createInput",
    value: function createInput() {
      var _this$props = this.props,
          elementName = _this$props.elementName,
          inputValue = _this$props.inputValue,
          placeHolder = _this$props.placeHolder;
      var attributes = {
        'aria-required': true,
        'aria-label': placeHolder,
        id: elementName,
        className: styles.input,
        name: elementName,
        onBlur: this.onBlur,
        onFocus: this.onFocus,
        onInput: this.onInput,
        type: 'text',
        ref: this.createInputReference
      };

      if (inputValue) {
        attributes.value = inputValue;
      } else {
        attributes.placeholder = placeHolder;
      }

      return /*#__PURE__*/React.createElement("input", attributes);
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: styles.fieldset
      }, this.createInput(), this.createLabel()));
    }
  }]);

  return TextInput;
}(Component);

TextInput.propTypes = {
  elementName: PropTypes.string.isRequired,
  inputValue: PropTypes.string,
  onInput: PropTypes.func,
  placeHolder: PropTypes.string
};
TextInput.defaultProps = {
  // README: inputValue is the value of the input.
  // setting this value to `undefined` or `null` will force this to be
  // and uncontrolled component, not what we want- throws warnings in
  // console
  // @see: https://facebook.github.io/react/docs/forms.html#controlled-components
  inputValue: '',
  onInput: function onInput() {
    /* no-op */
  },
  placeHolder: ''
};
export default TextInput;