function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import MediaButton from 'components/Buttons/MediaButton';
import TextInput from 'components/TextInput';
import Select from 'components/Select';
import { onPressPlay, onPressPause, onPressStop, onChangeBPM, onChangeSequence, onSequencesLoaded } from 'store/ducks/controls';
import styles from 'containers/controls.scss';

var ControlsContainer = /*#__PURE__*/function (_Component) {
  _inherits(ControlsContainer, _Component);

  var _super = _createSuper(ControlsContainer);

  function ControlsContainer(props) {
    var _this;

    _classCallCheck(this, ControlsContainer);

    _this = _super.call(this, props);
    _this.onClickStop = _this.onClickStop.bind(_assertThisInitialized(_this));
    _this.onClickPlay = _this.onClickPlay.bind(_assertThisInitialized(_this));
    _this.onClickPause = _this.onClickPause.bind(_assertThisInitialized(_this));
    _this.onBeatsChange = _this.onBeatsChange.bind(_assertThisInitialized(_this));
    _this.onSelectChange = _this.onSelectChange.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ControlsContainer, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // pseudo fetch...
      var _this$props = this.props,
          actions = _this$props.actions,
          controls = _this$props.controls;
      var testData = [{
        name: 'sequence 1',
        author: 'Foo',
        bpm: 128,
        sequence: {
          kickDrum: [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
          snareDrum: [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
          openHat: [0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
          closedHat: [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0]
        }
      }, {
        name: 'sequence 2',
        author: 'Bar',
        bpm: 110,
        sequence: {
          kickDrum: [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
          snareDrum: [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
          openHat: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          closedHat: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        }
      }, {
        name: 'sequence 3',
        author: 'Baz',
        bpm: 116,
        sequence: {
          kickDrum: [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          snareDrum: [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
          openHat: [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
          closedHat: [1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1]
        }
      }];
      actions.onSequencesLoaded(testData);
      actions.onChangeSequence(testData[0]);
    }
  }, {
    key: "onClickStop",
    value: function onClickStop(event) {
      event.preventDefault();
      var actions = this.props.actions;
      actions.onPressStop();
    }
  }, {
    key: "onClickPlay",
    value: function onClickPlay(event) {
      event.preventDefault();
      var actions = this.props.actions;
      actions.onPressPlay();
    }
  }, {
    key: "onClickPause",
    value: function onClickPause(event) {
      event.preventDefault();
      var actions = this.props.actions;
      actions.onPressPause();
    }
  }, {
    key: "onBeatsChange",
    value: function onBeatsChange(value) {
      // don't change for single entry
      if (value.length <= 2) {
        return;
      }

      var actions = this.props.actions;
      actions.onChangeBPM(value);
    }
  }, {
    key: "onSelectChange",
    value: function onSelectChange(option) {
      var actions = this.props.actions;
      actions.onChangeSequence(option);
    }
  }, {
    key: "renderControlBar",
    value: function renderControlBar() {
      var _this$props$controls = this.props.controls,
          currentSequence = _this$props$controls.currentSequence,
          playing = _this$props$controls.playing,
          sequenceList = _this$props$controls.sequenceList;
      var currentBPM = currentSequence ? currentSequence.bpm : null;
      console.log('??!!', currentSequence, currentBPM);
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(MediaButton, {
        ariaLabel: "stop",
        iconType: "stop",
        onClick: this.onClickStop
      }), !playing && /*#__PURE__*/React.createElement(MediaButton, {
        ariaLabel: "play",
        iconType: "play",
        onClick: this.onClickPlay
      }), playing && /*#__PURE__*/React.createElement(MediaButton, {
        ariaLabel: "pause",
        iconType: "pause",
        onClick: this.onClickPause
      }), /*#__PURE__*/React.createElement(TextInput, {
        elementName: "bpmInput",
        placeHolder: "Enter BPM",
        inputValue: "".concat(currentBPM),
        onInput: this.onBeatsChange
      }), /*#__PURE__*/React.createElement(Select, {
        options: sequenceList,
        selectedOption: currentSequence,
        onSelectChange: this.onSelectChange
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var title = this.props.title;
      return /*#__PURE__*/React.createElement("div", {
        className: styles.containerBase
      }, /*#__PURE__*/React.createElement("div", {
        className: styles.title
      }, title), /*#__PURE__*/React.createElement("div", {
        className: styles.controlBar
      }, this.renderControlBar()));
    }
  }]);

  return ControlsContainer;
}(Component);

ControlsContainer.propTypes = {
  title: PropTypes.string.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      onPressPlay: onPressPlay,
      onPressPause: onPressPause,
      onPressStop: onPressStop,
      onChangeBPM: onChangeBPM,
      onChangeSequence: onChangeSequence,
      onSequencesLoaded: onSequencesLoaded
    }, dispatch)
  };
}

function mapStateToProps(state) {
  return {
    controls: state.controls
  };
}

var ConnectedControlsContainer = connect(mapStateToProps, mapDispatchToProps)(ControlsContainer);
export default ConnectedControlsContainer;