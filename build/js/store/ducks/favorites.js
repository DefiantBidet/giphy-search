function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Model of Favorite
var favoriteModel = function favoriteModel(data) {
  return _objectSpread(_objectSpread({}, data), {}, {
    created_at: Date.now()
  });
}; // Initial State


var INITIAL_STATE = {
  saved: []
}; // Actions

var ON_SAVE_FAVORITE = 'favorites/ON_SAVE_GIF';
var ON_REMOVE_FAVORITE = 'favorites/ON_REMOVE_GIF'; // Redcuer

export default function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  switch (action.type) {
    case ON_SAVE_FAVORITE:
      {
        var saved = state.saved;
        var favoriteObj = action.data; // immutable array pattern

        var favoritesList = saved.slice();
        favoritesList.splice(favoritesList.length, 0, favoriteObj);
        return favoritesList;
      }

    case ON_REMOVE_FAVORITE:
      {
        var _saved = state.saved;
        var _favoriteObj = action.data;

        var favoriteIndex = _saved.findIndex(function (favorite) {
          return favorite.id === _favoriteObj.id;
        }); // immutable array pattern


        var _favoritesList = _saved.slice();

        _favoritesList.splice(favoriteIndex, 1);

        return _favoritesList;
      }

    default:
      return state;
  }
} // Action Creators

export function onSaveFavorite(data) {
  var newFavoriteModel = favoriteModel({
    id: data.id,
    slug: data.slug,
    url: data.url
  });
  return {
    type: ON_SAVE_FAVORITE,
    newFavoriteModel: newFavoriteModel
  };
}
export function onRemoveFavorite(data) {
  return {
    type: ON_REMOVE_FAVORITE,
    data: data
  };
}