function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// import
// Initial State
var INITIAL_STATE = {
  playing: false,
  bpm: null,
  currentSequence: null,
  sequenceList: [] // timestamp: undefined,

}; // Actions

var ON_PRESS_PLAY = 'controls/ON_PRESS_PLAY';
var ON_PRESS_PAUSE = 'controls/ON_PRESS_PAUSE';
var ON_PRESS_STOP = 'controls/ON_PRESS_STOP';
var ON_BPM_CHANGE = 'controls/ON_BPM_CHANGE';
var ON_SEQUENCE_CHANGE = 'controls/ON_SEQUENCE_CHANGE';
var ON_SEQUENCES_LOADED = 'controls/ON_SEQUENCES_LOADED';
export var actionsMap = {
  ON_PRESS_PLAY: ON_PRESS_PLAY,
  ON_PRESS_PAUSE: ON_PRESS_PAUSE,
  ON_PRESS_STOP: ON_PRESS_STOP,
  ON_BPM_CHANGE: ON_BPM_CHANGE,
  ON_SEQUENCE_CHANGE: ON_SEQUENCE_CHANGE,
  ON_SEQUENCES_LOADED: ON_SEQUENCES_LOADED
}; // Redcuer

export default function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  switch (action.type) {
    case ON_PRESS_PLAY:
      {
        var currentSequence = state.currentSequence;

        if (!currentSequence) {
          // no sequence defined - ignore press
          return state;
        }

        var newState = _objectSpread(_objectSpread({}, state), {}, {
          playing: true
        });

        return newState;
      }

    case ON_PRESS_PAUSE:
      {
        // TODO: handle timestamp - otherwise stop is the same.
        var _currentSequence = state.currentSequence;

        if (!_currentSequence) {
          // no sequence defined - ignore press
          return state;
        }

        var _newState = _objectSpread(_objectSpread({}, state), {}, {
          playing: false
        });

        return _newState;
      }

    case ON_PRESS_STOP:
      {
        var _currentSequence2 = state.currentSequence;

        if (!_currentSequence2) {
          // no sequence defined - ignore press
          return state;
        }

        var _newState2 = _objectSpread(_objectSpread({}, state), {}, {
          playing: false
        });

        return _newState2;
      }

    case ON_BPM_CHANGE:
      {
        var _currentSequence3 = state.currentSequence;
        var bpm = action.data.bpm;

        if (!_currentSequence3) {
          // no sequence defined - ignore change
          return state;
        }

        var _newState3 = _objectSpread(_objectSpread({}, state), {}, {
          bpm: bpm
        });

        return _newState3;
      }

    case ON_SEQUENCE_CHANGE:
      {
        var newSequence = action.data;

        var _newState4 = _objectSpread(_objectSpread({}, state), {}, {
          playing: false,
          bpm: newSequence.bpm,
          currentSequence: newSequence
        });

        return _newState4;
      }

    case ON_SEQUENCES_LOADED:
      {
        var list = action.data;

        var _newState5 = _objectSpread(_objectSpread({}, state), {}, {
          playing: false,
          sequenceList: list
        });

        return _newState5;
      }

    default:
      return state;
  }
} // Action Creators

export function onPressPlay(data) {
  return {
    type: ON_PRESS_PLAY,
    data: data
  };
}
export function onPressPause(data) {
  return {
    type: ON_PRESS_PAUSE,
    data: data
  };
}
export function onPressStop(data) {
  return {
    type: ON_PRESS_STOP,
    data: data
  };
}
export function onChangeBPM(data) {
  return {
    type: ON_BPM_CHANGE,
    data: data
  };
}
export function onChangeSequence(data) {
  return {
    type: ON_SEQUENCE_CHANGE,
    data: data
  };
}
export function onSequencesLoaded(data) {
  return {
    type: ON_SEQUENCES_LOADED,
    data: data
  };
}