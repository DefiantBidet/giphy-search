import reducer, { actionsMap, onPressPlay, onPressPause, onPressStop, onChangeBPM, onChangeSequence, onSequenceLoaded } from './controls'; // reducer

describe('controls store reducer', function () {
  it('should return the initial state', function () {
    expect(reducer(undefined, {})).toEqual({
      playing: false,
      bpm: null,
      sequence: null
    });
  });
  it.skip('ON_PRESS_PLAY should return default state if no sequence', function () {
    expect(reducer()).toEqual();
  });
  it.skip('ON_PRESS_PLAY should return new state', function () {
    expect(reducer()).toEqual();
  });
  it.skip('ON_PRESS_PAUSE should return default state if no sequence', function () {
    expect(reducer()).toEqual();
  });
  it.skip('ON_PRESS_PAUSE should return new state', function () {
    expect(reducer()).toEqual();
  });
  it.skip('ON_PRESS_STOP should return default state if no sequence', function () {
    expect(reducer()).toEqual();
  });
  it.skip('ON_PRESS_STOP should return new state', function () {
    expect(reducer()).toEqual();
  });
  it.skip('should handle ON_BPM_CHANGE', function () {
    expect(reducer()).toEqual();
  });
  it.skip('should handle ON_SEQUENCE_CHANGE', function () {
    expect(reducer()).toEqual();
  });
  it.skip('should handle ON_SEQUENCE_LOADED', function () {
    expect(reducer()).toEqual();
  });
}); // action creators

describe('controls store action creators', function () {
  it('should create onPressPlay action', function () {
    var expectedAction = {
      type: actionsMap.ON_PRESS_PLAY
    };
    expect(onPressPlay()).toEqual(expectedAction);
  });
  it('should create onPressPause action', function () {
    var expectedAction = {
      type: actionsMap.ON_PRESS_PAUSE
    };
    expect(onPressPause()).toEqual(expectedAction);
  });
  it('should create onPressStop action', function () {
    var expectedAction = {
      type: actionsMap.ON_PRESS_STOP
    };
    expect(onPressStop()).toEqual(expectedAction);
  });
  it('should create onChangeBPM action', function () {
    var bpm = '150';
    var expectedAction = {
      type: actionsMap.ON_BPM_CHANGE,
      data: bpm
    };
    expect(onChangeBPM(bpm)).toEqual(expectedAction);
  });
  it('should create onChangeSequence action', function () {
    var expectedAction = {
      type: actionsMap.ON_SEQUENCE_CHANGE
    };
    expect(onChangeSequence()).toEqual(expectedAction);
  });
  it('should create onSequenceLoaded action', function () {
    var sequence = {
      bpm: 180,
      data: {}
    };
    var expectedAction = {
      type: actionsMap.ON_SEQUENCE_LOADED,
      data: sequence
    };
    expect(onSequenceLoaded(sequence)).toEqual(expectedAction);
  });
});
describe.skip('todos reducer', function () {
  it('should handle ADD_TODO', function () {
    expect(reducer([], {
      type: types.ADD_TODO,
      text: 'Run the tests'
    })).toEqual([{
      text: 'Run the tests',
      completed: false,
      id: 0
    }]);
    expect(reducer([{
      text: 'Use Redux',
      completed: false,
      id: 0
    }], {
      type: types.ADD_TODO,
      text: 'Run the tests'
    })).toEqual([{
      text: 'Run the tests',
      completed: false,
      id: 1
    }, {
      text: 'Use Redux',
      completed: false,
      id: 0
    }]);
  });
});