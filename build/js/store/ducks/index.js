import favorites from './favorites'; // Reducers

var reducers = {
  favorites: favorites
}; // Default Export

export default reducers;